function diss_mean = computeMeanDissimilarity(maps)
% Compute mean dissimilarity between groups, averaged over all maps.
%
% Input:
% - maps: matrix with size [n_channels, n_maps, n_groups].
%
% Output:
% - diss_mean: average value of dissimilarity between groups. This is a
% measure of how similar all maps in all groups are.

[n_channels, n_maps, n_groups] = size(maps);

% Loop over maps.
diss_maps = zeros(n_maps, 1);
for k = 1:n_maps
    maps_k = squeeze(maps(:, k, :));
    diss = computeDissimilarityIndex(maps_k);
    diss_maps(k) = mean(diss(:));
end

diss_mean = mean(diss_maps);

end
