%% ANOVA function
function p = computeTANOVA(MS1, MS2, n_permutations, ignore_polarity)

% MS1 = first MS map (1D vector with length n_channels).
% MS2 = second MS map (to be compared with group1).
% n_permutations: the number of permutations to do.

if nargin < 3 || isempty(n_permutations); n_permutations = 5000; end 
if nargin < 4 || isempty(ignore_polarity); ignore_polarity = true; end 

% Make sure they are column vectors.
MS1 = MS1(:);
MS2 = MS2(:);

% Ignore polarity.
if ignore_polarity
    % Flip polarity of MS1 if it decreases the dissimilarity.
    MS1 = matchPolarity(MS1, MS2);
end

gdReal = compute_gd(MS1, MS2);
allConditions = [MS1; MS2];

% Create permutations of the data and recompute the GD metric.
gdPerm = nan(1, n_permutations);
for k = 1:n_permutations
    ind = randperm(size(allConditions,1));
    allConditions_perm = allConditions(ind,:);
    group1_perm = allConditions_perm(1:length(MS1));
    group2_perm = allConditions_perm(length(MS1)+1:end);
    gdPerm(k) = compute_gd(group1_perm, group2_perm);
    clear ind
end
p = mean(gdPerm >= gdReal);

end

function gd = compute_gd(u, v)

allConditions     = horzcat(u,v);
meanAllConditions = mean(allConditions,2);
residual          = allConditions-meanAllConditions;
meanRes1          = sqrt(mean(residual(:,1).^2));
meanRes2          = sqrt(mean(residual(:,2).^2));
gd                = meanRes1 + meanRes2;

end