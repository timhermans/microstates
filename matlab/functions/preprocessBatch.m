function preprocessBatch(i_batch, num_batches)
% This function loops over a batch of EDF files, reads the EEG data, 
% filters the data and saves it as a .set file for EEGLAB.
% Useful function to preprocess EEG files in parallel.

if nargin < 2 || isempty(num_batches)
    % By default, use 20 batches.
    num_batches = max(20, i_batch);
end

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Where to save the output files.
output_dir = fullfile(paths.output_root_dir, 'filtered_2');

% Create output dir if it does not exist.
if ~isfolder(output_dir)
   mkdir(output_dir)
end

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
[ALLEEG EEG CURRENTSET] = eeglab('nogui');

% Load Excel file.
T = readtable(paths.recording_data_file);

% Get chanlocs filepath.
chanlocs_file = paths.chanlocs_file;

% Loop over a batch of rows in table.
nrows = height(T);
batch_size = ceil(nrows/num_batches);
start = (i_batch - 1)*batch_size + 1;
stop = min(start + batch_size - 1, nrows);
for row = start:stop 
    fprintf('%d/%d...\n', row - start + 1, stop - start + 1) 
    
    % Create filepath.
    filename = T.filename{row};
    dataset = T.dataset{row}; 
    
    if strcmp(dataset, 'D1')
        edf_dir = paths.D1_data_dir; 
    elseif strcmp(dataset, 'D2')
        edf_dir = paths.D2_data_dir; 
    end
    filepath = fullfile(edf_dir, filename);
    
    % Preprocess.
%     try
    EEG = preprocessEEG(filepath, output_dir, chanlocs_file);
%     catch
%         fprintf('Error while processing %s. Skipping...\n', filepath) 
%         continue
%     end
end   

