function setPlotDefaults(scale)

if nargin < 1 || isempty(scale)
    scale = 0.8;
end

set(groot,'defaultLineLineWidth',2.5*scale) 
set(0,'defaultFigureColor', 'w')
set(0,'DefaultaxesLineWidth', 1.5*scale) 
set(0,'DefaultaxesFontSize', 14*scale) 
set(0,'DefaultaxesFontWeight', 'bold') 
set(0,'DefaultTextFontSize', 14*scale) 

end