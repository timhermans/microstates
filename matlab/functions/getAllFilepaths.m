function filepaths = getAllFilepaths(set_dir, outcome, pma_group, ...
    sleep_stage, T)
% Get a list of all .set filepaths for a given group and condition.
%
% Input:
% - set_dir (str): directory path in which the .set files are located.
% - outcome (str, optional): outcome group to extract.
% - pma_group (str, optional): PMA group to extract.
% - sleep_stage (str): sleep stage to extract.
% - T (table, optional): table with recording data. If not specified, will
% load this one (this is slow, so if you call this function multiple times,
% its better to preload the table and pass it when calling this function).
%
% Output:
% - filepaths (cell): cell array with filepaths to .set files.
if nargin < 5
    T = [];
end

if nargin < 3
    pma_group = [];
end

if nargin < 2
    outcome = [];
end

if isempty(T)
    % Load patient data.
    paths = getPaths();
    T = readtable(paths.recording_data_file);
end

if ~isempty(outcome)
    % Select outcome group.
    T = T(strcmp(T.outcome, outcome), :);
end

if ~isempty(pma_group)
    % Select PMA group.
    T = T(strcmp(T.PMA_group, pma_group), :);
end

% Create cell array with all filepaths in the group.
filepaths = cell(height(T), 1);
for i_rec = 1:height(T)
    row = T(i_rec, :);
    [~, rec_name, ~] = fileparts(row.filename{1});
    filepaths{i_rec} = fullfile(set_dir, [rec_name, '_preprocessed_', sleep_stage, '.set']);
end

end