function plotMap(map, chanlocs, background, use_topoplot, map_limits)
% Topoplot.
% 
% Input:
% - map : vector with size (1, n_channels) containing the channel
% potentials.
% - chanlocs : channel locations: (1, n_channels) struct with at least X, Y
% and Z fields zontaining the coordinates of the channels. By defaults, a 9
% channel location file is read and used.
% - background : color for the background. By default, it is grey.  

if ~any(size(map) == 1)
    error('Invalid map with size [%s]. Should be size [1  n_channels].', num2str(size(map)))
end

% Transpose if needed.
if diff(size(map)) < 0
    map = map';
end

if nargin < 2 || isempty(chanlocs)
    % Load default chanlocs.
    paths = getPaths();
    chanlocs = readlocs(paths.chanlocs_file);
end
if nargin < 3 || isempty(background)
    background = [1, 1, 1]*0.7;  % Light grey.
end

if length(map) ~= length(chanlocs)
    error('Number of channels in map (%d) does not agree with the number of channels in chanlocs (%d).',...
        length(map), length(chanlocs))
end

% Get coordinates.
X = cell2mat({chanlocs.X});
Y = cell2mat({chanlocs.Y});
Z = cell2mat({chanlocs.Z});

% Plot.
if use_topoplot
    topoplot(double(map), chanlocs, 'electrodes', 'off', 'whitebk', 'on', 'style', 'both', 'maplimits', map_limits)
else
    dspCMap(double(map),[X;Y;Z],'NoScale','Resolution',3, 'Background', background);
end

end