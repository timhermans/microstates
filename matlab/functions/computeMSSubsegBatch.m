function computeMSSubsegBatch(i_batch, num_batches)
% This function loops over a batch of preprocessed .set files, 
% performs MS analysis and saves it to new files.
% Useful function to preprocess EEG files in parallel.

% Custom cluster parameters (use default for non-custom parameters).
CustomClustPars = struct;

if nargin < 2 || isempty(num_batches)
    % By default, use 20 batches.
    num_batches = max(20, i_batch);
end

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with preprocessed .set files.
data_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs');

% Where to save the output files.
output_dir = fullfile(data_dir, 'microstates', 'subseg');

% Create output dir if it does not exist.
if ~isfolder(output_dir)
    mkdir(output_dir)
end

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
[ALLEEG EEG CURRENTSET] = eeglab('nogui');

% List all datafiles.
all_files = dir(fullfile(data_dir, '*.set'));

% Loop over a batch of files.
nfiles = length(all_files);
batch_size = ceil(nfiles/num_batches);
start = (i_batch - 1)*batch_size + 1;
stop = min(start + batch_size - 1, nfiles);
for i_file = start:stop
    filepath_set = fullfile(data_dir, all_files(i_file).name);
        
    EEG = pop_loadset(filepath_set);    % Basic file read
        
    seg_times = [
        0, 149.995;
        150, 300];
    for ii = 1:size(seg_times, 1)
        fname = all_files(i_file).name;
        fname = replace(fname, '.set', sprintf('_subseg_%d.set', ii));
        filepath_out = fullfile(output_dir, fname);

        if isfile(filepath_out)
            fprintf('File %s already exists. Skipping...\n', filepath_out)
            continue
        end
        
        seg_ii = seg_times(ii, :);
        EEG_ii = pop_select( EEG, 'time', seg_ii);

        [ALLEEG, EEG_ii, CURRENTSET] = pop_newset(ALLEEG, EEG_ii, 1, 'gui','off');        % And make this a new set
        ALLEEG = eeg_store(ALLEEG, EEG_ii, CURRENTSET);  % Store the thing

        fprintf(1,'Clustering dataset %s (%i/%i)\n',EEG_ii.setname,i_file - start + 1, stop - start + 1); % Some info for the impatient user
        [ALLEEG, EEG_ii] = computeMS(ALLEEG, CURRENTSET, CustomClustPars);

        % save results
        pop_saveset(EEG_ii, 'filename', fname, 'filepath', output_dir, 'savemode', 'onefile');
    end
end
end
