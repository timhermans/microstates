function [GMD, pvalues] = plotGDM(MS_all, group_labels, sort_maps, ...
    compute_p, n_randomnizations, add_colorbar)
% Plot global dissimilarity matrix.
%
% Input:
% - MS_all : matric with MS maps with shape (n_channels, n_maps, n_groups).
% - group_labels : cell array with length n_groups, containing labels for 
% each of the groups.
% - sort_maps : bool, if true, sorts the maps such that the global 
% dissimilariy between groups is as small as possible. Defaults to true.
% - compute_p: bool, if true, computes pvalues in the diagonal regions to
% compare maps between groups (might be slow). Defaults to false.
% - n_randomnizations: int, the number of permutations in the permutations
% test for computing the pvalue.
% - add_colorbar: bool, if true, adds a colorbar to the plot.

if nargin < 3 || isempty(sort_maps); sort_maps = true; end
if nargin < 4 || isempty(compute_p); compute_p = false; end
if nargin < 5 || isempty(n_randomnizations); n_randomnizations = 5000; end
if nargin < 6 || isempty(add_colorbar); add_colorbar = true; end

% Number of groups to compare.
[n_channels, n_maps, n_groups] = size(MS_all);

% Sort.
if sort_maps
    MS_all = sortMaps(MS_all);
end

% Arange in grid.
MS_Total = [];
labels = {};
for i_map = 1:n_maps
    for i_group = 1:n_groups
        MS_Total(:, end+1) = MS_all(:, i_map, i_group);
        labels{end+1} = group_labels{i_group};
    end
end

% Compute GMD.
GMD = computeDissimilarityIndex(MS_Total);

% Plot.
imagesc(GMD);  
set(gca,'dataAspectRatio',[1 1 1])

if add_colorbar
    cbar = colorbar;
    caxis([0 1.5])
    ylabel(cbar, 'Dissimilarity index (-)', 'FontSize', 10, 'FontWeight', 'bold')
end

set(gca, 'XTick', 1:length(labels), ...                             % Change the axes tick marks
'XTickLabel', labels, ...  %   and tick labels
'YTick', 1:length(labels), ...
'YTickLabel', labels, ...
'TickLength', [0 0],'XAxisLocation','top','Box','on');
for k=1:n_maps
    line([n_groups*k+0.5 n_groups*k+0.5],[0 n_maps*n_groups+1],'Color','k','LineWidth',2.5);
    line([0, n_maps*n_groups+1],[n_groups*k+0.5 n_groups*k+0.5],'Color','k','LineWidth',2.5);
end

% Compute pvalue for TANOVA.
pvalues = nan(size(GMD));
if compute_p
   disp('Performing TANOVA for diagonal combinations...')
    for i_map = 1:n_maps
        for i_group_1 = 1:n_groups
            for i_group_2 = (i_group_1+1):n_groups
                MS_1 = MS_all(:, i_map, i_group_1);
                MS_2 = MS_all(:, i_map, i_group_2);
%                 p_val_i = computeTANOVA(MS_1, MS_2, n_randomnizations);
                p_val_i = computeTANOVA2([MS_1, MS_2], n_randomnizations);
%                 p_val_i = itab_groupanova(MS_1, MS_2);
                pvalues((i_map - 1)*n_groups + i_group_1, (i_map - 1)*n_groups + i_group_2) = p_val_i;
            end
        end
    end

%     disp('Performing TANOVA for all combinations...')
%     for ii = 1:size(MS_Total, 2)
%         for jj = ii:size(MS_Total, 2)
%             MS_1 = MS_Total(:, ii);
%             MS_2 = MS_Total(:, jj);
%             p_val_i = computeTANOVA2([MS_1, MS_2], n_randomnizations);
%             pvalues(ii, jj) = p_val_i;
%         end
%     end
    
    % Annotate pvalue in plot.
    [r_p, c_p] = find(pvalues < 0.05);
    for i_p = 1:length(r_p)
        text(c_p(i_p), r_p(i_p), '*', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', ...
            'fontsize', get(0,'defaultAxesFontSize')*1.2)
    end
end
    
end