function MS_1 = matchPolarity(MS_1, MS_2)

diss1 = computeDissimilarityIndex([-MS_1, MS_2], false);
diss2 = computeDissimilarityIndex([MS_1, MS_2], false);
if diss1(1, 2) < diss2(1, 2)
    % Flip polarity if it makes dissimilarity smaller.
    MS_1 = -MS_1;
end

end