function W = CalcDispersion(X,A,L)
% Inputs:
%  X       - EEG (channels x samples).
%  A       - Cell array of spatial distributions (channels x K), for
%            different number of microstates. Can also be single 2D array.
%  L       - Cell array of labels of the most active microstate at each
%            timepoint (1 x samples). Can also be single 1D array.
%
%  Outputs:
%  W       - Dispersion of clusters.

K = size(A,2);   % number of clusters (microstates)
Dk = nan(K,1);
Nk = nan(K,1);
max_samp = 30000;  % Maximum number of samples (time points) to use.

% Use a random subset to speed up computations.
if size(X, 2) > max_samp
    rng(43)  % Seed.
    idx = randsample(size(X,2), max_samp);
    X = X(:, idx);
    L = L(idx);
end
for k = 1:K
    cluster = X(:,L==k);
    
    Nk(k) = size(cluster,2);
    
    %sum of pair-wise distance between all maps of cluster k
%     Dk(k) = 0;
%     for i = 1:Nk(k)
%         ci = cluster(:, i);
%         di = ci - cluster;
%         ssq = dot(di, di);
%         Dk(k) = Dk(k) + sum(ssq);
%     end
    clstrsq = dot(cluster,cluster,1);
    Dk(k) = sum(sum(bsxfun(@plus,clstrsq',clstrsq)-2*(cluster'*cluster)));  
end
idx = Nk ~= 0; % in case of empty clusters
W = (1./(2*Nk(idx)))' * Dk(idx);
end