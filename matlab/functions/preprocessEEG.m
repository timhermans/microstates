function EEG = preprocessEEG(filepath, output_dir, chanlocs_file)

this_dir = fileparts(mfilename('fullpath'));

if nargin < 3 || isempty(chanlocs_file)
    % Channels to read.
    chanlocs_dir = fullfile(fileparts(this_dir), 'channel_locations');
    chanlocs_file = fullfile(chanlocs_dir, 'Channel_Location_9electrodes.ced');
end

% Create output filepath.
[~, filename, ~] = fileparts(filepath);
filepath_out = fullfile(output_dir, [filename, '.set']);

% Skip if already processed.
if isfile(filepath_out)
    fprintf('File %s already exists. Skipping...\n', filepath_out)
    EEG = [];
    return
end

% Channels to read.
chanlocs = readlocs(chanlocs_file);

% Open an EDF file and read header.
fid = fopen(filepath);
hdr = readEdfHeader(fid);
edf_labels = hdr.label;
fclose(fid);

% Get the indices of the channels in the EDF.
chanidx = NaN(length(chanlocs), 1);
channel_labels = cell(length(chanlocs), 1);
for i = 1:length(chanidx)
    label = chanlocs(i).labels;
    channel_labels{i} = label;
    idx = find(strcmp(edf_labels, label));
    if isempty(idx)
        error('Channel %s not found in EDF channels [%s].', label, strjoin(edf_labels, ','))
    end
    chanidx(i) = idx;
end

% Read the EEG channels.
EEG = pop_biosig(filepath, 'channels', chanidx);

% Set the channel locations. Note that the channel names as loaded by
% pop_biosig may be incorrect when using the 'channels' parameter.
% The signals will be read in the order defined by 'channels'. Since this
% order was determined by the channel location file, we can just replace
% the channel locations in the loaded EEG by the channel locations of the
% channel location file.
EEG.chanlocs = pop_chanedit(EEG.chanlocs, 'load', {chanlocs_file});
fs = EEG.srate;

% Reference to average reference.
EEG.data = EEG.data - mean(EEG.data, 1);

% Filter the signals.
stop_low = 1.5;
fc_low = 2;
fc_high = 20;
stop_high = 21;
    
filt_path = fullfile(this_dir, 'cache', sprintf('bpfilt_%.2f-%.2f_%dHz.mat', fc_low, fc_high, fs));
if isfile(filt_path)
    load(filt_path, 'bpfilt')
else
    ripple = 1;
    db_low = 40;
    db_high = 40;

     bpfilt = designfilt('bandpassfir',...
     'PassbandFrequency1',fc_low,...
     'PassbandFrequency2',fc_high, ...
     'StopbandFrequency1',stop_low,...
     'StopbandFrequency2',stop_high,...
     'PassbandRipple',ripple,...
     'StopbandAttenuation1',db_low,...
     'StopbandAttenuation2',db_high,...
     'SampleRate',EEG.srate);
 
     if ~isfolder(fileparts(filt_path))
         mkdir(fileparts(filt_path))
     end
    save(filt_path, 'bpfilt');
end

EEG_filt_data = filtfilt(bpfilt, double(EEG.data)')';
EEG.data = EEG_filt_data;

% Resample to 100 Hz.
fs_res = 100;
EEG = pop_resample(EEG, fs_res);

% Save as .SET.
pop_saveset(EEG, ...
    'filepath', filepath_out, ...
    'check', 'on', ...
    'savemode', 'onefile');

end