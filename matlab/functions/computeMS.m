function [ALLEEG, EEG] = computeMS(ALLEEG, CURRENTSET, CustomClustPars, CustomFitPars)
% Compute miscrostates maps and KL criterion on the CURRENTSET in ALLEEG.
%
% Input:
% - ALLEEG : struct from EEGLAB containing the EEG to process.
% - CURRENTSET : index pointing to the EEG in ALLEEG which to process.
% - CustomClustPars : optional custom clustering parameters (will override
% defaults).
% - CustomFitPars : optional custom fitting parameters (will override
% defaults).
%
% Output:
% - ALLEEG : updated EEGLAB struct with computed MS and metrics.
% - EEG : the EEG dataset in ALLEEG for which the microstates were
% computed.

% Inputs.
if nargin < 4 || isempty(CustomFitPars)
    CustomFitPars = struct;
end
if nargin < 3 || isempty(CustomClustPars)
    CustomClustPars = struct;
end

% Defining default clustering parameters
ClustPars.MinClasses = 3;         % minimum number of microstates
ClustPars.MaxClasses = 15;        % maximum number of microstates
ClustPars.GFPPeaks = true;        % use GFP peaks as inputs of clustering algorithm
ClustPars.IgnorePolarity = true;
ClustPars.MaxMaps = inf;          % number of time sample data feeded to clusterig algorithm. Randomly selects time stamps at which to extract maps for clustering.
ClustPars.Restarts = 20;          % number of repeatition for modified k-means 
ClustPars.UseAAHC = false;        % if true, TAAGC will be applied, else modified k-means.

% Defining default fitting parameters
FitPars.b = 30;                   % the window size for label smoothing (only applicable when not fitting to GFP peaks only)
FitPars.lambda = 1;               % the non-smoothness penalty when smoothing labels.
FitPars.PeakFit = true;           % if true, microstate labels between GFP peaks are defined by nearest neighbor interpolation.
FitPars.BControl = true;          % if true, does not assign periods where microstates are truncated.

% Update clustering parameters with custom parameters defined by user.
ClustPars = updateStruct(ClustPars, CustomClustPars);
FitPars = updateStruct(FitPars, CustomFitPars);

% Process.
tmpEEG = eeg_retrieve(ALLEEG, CURRENTSET); % the EEG we want to work with
tmpEEG = pop_FindMSTemplates(tmpEEG, ClustPars); % This is the actual clustering within subjects
ALLEEG = eeg_store(ALLEEG, tmpEEG, CURRENTSET); % Done, we just need to store this

% Set fit parameters.
ALLEEG(CURRENTSET).msinfo.FitParams = FitPars;
for k = ClustPars.MinClasses:ClustPars.MaxClasses
    % - MSClass: N timepoints x N Segments matrix of momentary labels
    % - gfp: N timepoints x N Segments matrix of momentary gFP values
    % - fit: Relative variance explained by the model
    [MSClass, gfp, fit] = AssignMStates(ALLEEG(CURRENTSET), ALLEEG(CURRENTSET).msinfo.MSMaps(k).Maps, ALLEEG(CURRENTSET).msinfo.FitParams, true);
    ALLEEG(CURRENTSET).msinfo.L_all{k,1} = MSClass';  % L_all:Lables: segments x time
    ALLEEG(CURRENTSET).msinfo.gfp(1,:) = gfp;  % segments x time
    ALLEEG(CURRENTSET).msinfo.fit(k,:) = fit;  
    ALLEEG(CURRENTSET).msinfo.A_all{k,1} = ALLEEG(CURRENTSET).msinfo.MSMaps(k).Maps';  % A_all:MS Maps: channels x maps

    % Compution quality measure W (dispersion). W trends toward 0 as the quality of the clustering results increases
    data = ALLEEG(CURRENTSET).data;
    data = data(:,ALLEEG(CURRENTSET).msinfo.L_all{k,1}~=0);
    tempL = ALLEEG(CURRENTSET).msinfo.L_all{k,1}(ALLEEG(CURRENTSET).msinfo.L_all{k,1}~=0);
    w = CalcDispersion(data, ALLEEG(CURRENTSET).msinfo.A_all{k,1}, tempL);
    W(k-(ClustPars.MinClasses-1)) = w; % Save dispersion for each k (number of kmeans clusters).
    clear data;
end
% Krzanowski-Lai (KL) criterion: point where W has elbow point.
NumK = length(W);  % Number of k's tried.
C = ALLEEG(CURRENTSET).nbchan;
for seg = 2:NumK-1 % We need to compute the curvature, so we'll work with derivatives, so cannot compute for first and last k.
    DIFFk = (seg-1)^(2/C)*W(seg-1) - seg^(2/C)*W(seg);
    DIFFk1 = seg^(2/C)*W(seg) - (seg+1)^(2/C)*W(seg+1);
    KL(seg) = abs(DIFFk/DIFFk1);
    ALLEEG(CURRENTSET).msinfo.KL{seg,1} = abs(DIFFk/DIFFk1);
end

EEG = eeg_retrieve(ALLEEG,CURRENTSET);

end