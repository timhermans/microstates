function [EEGOUT] = findGroupMSTemplates(ALLEEG, CURRENTSET, MeanSetName, Restarts, ...
    IgnorePolarity, ShowPlots, ShowWhenDone)
% Find group MS templates by applying k-means to per subject maps.
%
%   "ALLEEG" 
%   -> ALLEEG structure with all the EEGs that may be analysed
%
%   "CURRENTSET" 
%   -> Index of selected EEGs. By default uses all sets in ALLEEG.
%
%   "Name of mean" / MeanSetName
%   -> Name of the new dataset returned by EEGOUT
% 
%   "Restarts"
%   -> The number of reiterations (defaults to 20)
%
%   "IgnorPolarity"
%   -> Ignores polarity when clustering (defaults to true).
% 
%   "ShowPlots"
%   -> Show plots of the clustering (defaults to false).
%
% Output:
%
%   "EEGOUT" 
%   -> EEG structure with the EEG containing the new cluster centers
%
% This function is based on pop_CombMSTemplates, but replaces the
% permutation averaging with k-means clustering.
% Author: Tim Hermans (tim.hermans@esat.kuleuven.be)

% Default inputs.
if nargin < 2 || isempty(CURRENTSET); CURRENTSET = 1:length(ALLEEG); end
if nargin < 3 || isempty(MeanSetName);  MeanSetName = 'GrandMean';  end
if nargin < 4 || isempty(Restarts);  Restarts = 20;  end
if nargin < 5 || isempty(IgnorePolarity);  IgnorePolarity = true;  end
if nargin < 6 || isempty(ShowPlots);  ShowPlots = false;  end
if nargin < 7 || isempty(ShowWhenDone);  ShowWhenDone = false;  end

if IgnorePolarity == true
    flags = '';
else
    flags = 'p';
end

SelectedSet = CURRENTSET;

% Input checks.
if numel(SelectedSet) < 2
    error('You must select at least two sets of microstate maps.');
end
if ~isfield(ALLEEG(SelectedSet(1)),'msinfo')
    error('Microstate info not found in dataset %', ALLEEG(SelectedSet(1)).setname);
end

% Extract MS parameters.
MinClasses     = ALLEEG(SelectedSet(1)).msinfo.ClustPar.MinClasses;
MaxClasses     = ALLEEG(SelectedSet(1)).msinfo.ClustPar.MaxClasses;
IgnorePolarity_ = ALLEEG(SelectedSet(1)).msinfo.ClustPar.IgnorePolarity;
GFPPeaks       = ALLEEG(SelectedSet(1)).msinfo.ClustPar.GFPPeaks;

% Init.
allchans  = { };
children  = cell(length(SelectedSet),1);
keepindex = 0;

% Loop over sets for averaging.
for index = 1:length(SelectedSet)
    % Check is set is valid.
    if ~isfield(ALLEEG(SelectedSet(index)),'msinfo')
        error('Microstate info not found in dataset %',ALLEEG(SelectedSet(index)).setname); 
    end
    if  MinClasses     ~= ALLEEG(SelectedSet(index)).msinfo.ClustPar.MinClasses || ...
        MaxClasses     ~= ALLEEG(SelectedSet(index)).msinfo.ClustPar.MaxClasses || ...
        IgnorePolarity_ ~= ALLEEG(SelectedSet(index)).msinfo.ClustPar.IgnorePolarity || ...
        GFPPeaks       ~= ALLEEG(SelectedSet(index)).msinfo.ClustPar.GFPPeaks
        error('Microstate parameters differ between datasets');
    end
    
    % Collect data.
    children(index) = {ALLEEG(SelectedSet(index)).setname};
    tmpchanlocs = ALLEEG(SelectedSet(index)).chanlocs;
    tmpchans = { tmpchanlocs.labels };
    allchans = unique_bc([ allchans {tmpchanlocs.labels}]);

    if length(allchans) == length(tmpchans)
        keepindex = index;
    end
end
if keepindex
    tmpchanlocs = ALLEEG(SelectedSet(keepindex)).chanlocs; 
end

% Ready to go, it seems. 

% Create msinfo struct for the set with group templates.
msinfo.children = children;
msinfo.ClustPar   = ALLEEG(SelectedSet(1)).msinfo.ClustPar;

% Loop over classes.
for n = MinClasses:MaxClasses
    % Now we create a matrix of subject x classes x channels.
    MapsToSort = nan(numel(SelectedSet), n, numel(tmpchanlocs));
    
    % Loop over subjects.
    for index = 1:length(SelectedSet)
        % Here we go to the common set of channels
        dummy.data     = ALLEEG(SelectedSet(index)).msinfo.MSMaps(n).Maps;
        dummy.chanlocs = ALLEEG(SelectedSet(index)).chanlocs;
        out = pop_interp(dummy,tmpchanlocs,'spherical');
        MapsToSort(index,:,:) = out.data;
    end
    
    % To shape (subjects*maps x channels).
    MapsToCluster = reshape(MapsToSort, [], size(MapsToSort, 3));
    [BestMeanMap,b_ind,b_loading,exp_var] = eeg_kMeans(MapsToCluster, n, Restarts, [], flags);

    if ShowPlots
        figure('color', 'w');
        plotClustering(MapsToCluster, BestMeanMap, b_ind, b_loading, IgnorePolarity)
    end
    
    % Save maps.
    msinfo.MSMaps(n).Maps = BestMeanMap;
    msinfo.MSMaps(n).ExpVar = exp_var;
    msinfo.MSMaps(n).ColorMap = lines(n);
    msinfo.MSMaps(n).SortedBy = 'none';
    msinfo.MSMaps(n).SortMode = 'none';
end

EEGOUT = eeg_emptyset();
EEGOUT.chanlocs = tmpchanlocs;
EEGOUT.data = zeros(numel(EEGOUT.chanlocs),MaxClasses,MaxClasses);
EEGOUT.msinfo = msinfo;
    
for n = MinClasses:MaxClasses
    EEGOUT.data(:,1:n,n) = msinfo.MSMaps(n).Maps';
end

EEGOUT.setname     = MeanSetName;
EEGOUT.nbchan      = size(EEGOUT.data,1);
EEGOUT.trials      = size(EEGOUT.data,3);
EEGOUT.pnts        = size(EEGOUT.data,2);
EEGOUT.srate       = 1;
EEGOUT.xmin        = 1;
EEGOUT.times       = 1:EEGOUT.pnts;
EEGOUT.xmax        = EEGOUT.times(end);

if ShowWhenDone == true
    pop_ShowIndMSMaps(EEGOUT);
end

end
