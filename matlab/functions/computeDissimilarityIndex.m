function GMD = computeDissimilarityIndex(maps, ignore_polarity)
% Compute the global dissimilarity index between all pairs of maps.
%
% Input:
% - maps : matrix with MS maps with shape (n_channels, n_maps).
% - ignore_polarity : boolean, defaults to true.
% 
% Output:
% - GMD : matrix with shape (n_maps, n_maps) with the global dissimilarity
% indices for each pair of maps in the input. E.g. GMD(1, 3) corresponds to
% the dissimilarity between the first and third column in `maps`.

if nargin < 2 || isempty(ignore_polarity)
    ignore_polarity = true;
end

[n_channels, n_maps] = size(maps);

% Normalize.
MS_Total = (maps - mean(maps,1)) ./ std(maps,1);

% Compute global dissimilarity.
for k = 1:n_maps
    GMD(k,:) = sqrt(mean((MS_Total - repmat(MS_Total(:,k),1,n_maps)).^2 ));
end

if ignore_polarity
    % Compute dissimilarity with inversed polarity.
    for k = 1:n_maps
        GMDinvpol(k,:) = sqrt(mean((MS_Total - repmat(-MS_Total(:,k),1,n_maps)).^2));
    end

    % Select best matching polarity.
    idx = GMDinvpol < GMD;
    GMD(idx) = GMDinvpol(idx);
end

end