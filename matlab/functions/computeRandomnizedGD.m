% Compute the effect sizes of n_randomnizations.

% Create dummy data.
clear

seed = 43;
n_randomnizations = 100;
ignore_polarity = true;
restarts = [];
rng(seed)

group_sizes = [10, 12, 15, 8]*4;
n_groups = length(group_sizes);
n_channels = 9;
n_maps = 4;
grouped_data = cell(n_groups, 1);
for i_group = 1:n_groups
    group_data = rand(group_sizes(i_group), n_maps, n_channels);
    group_data(:, 1, i_group) = 5;
    grouped_data{i_group, 1} = (group_data - mean(group_data, 3))./std(group_data, 1, 3);
end

tic

if isempty(restarts);  restarts = 20;  end

if ignore_polarity == true
    flags = '';
else
    flags = 'p';
end

if ~isempty(seed)
    % Seed random generator.
    rng(seed);
end

% Transpose if needed (must be a column).
if diff(size(grouped_data)) > 0
    grouped_data = grouped_data';
end

if size(grouped_data, 2) ~= 1
    error('Unexpected size of `grouped_data`. Size must be [n_groups  1]. Got size [%s].', num2str(size(grouped_data)))
end
if size(grouped_data, 1) < 2
    error('Pass at least 2 groups. Got %d group.', size(grouped_data, 1));
end

% Number of groups.
n_groups = numel(grouped_data);

% Get group sizes.
group_sizes = cellfun(@(c) size(c, 1), grouped_data);

% Concatenate all data (n_subjects_tot, n_maps, n_channels).
ungrouped_data = cell2mat(grouped_data);

% Sizes. 
[n_subjects, n_maps, n_channels] = size(ungrouped_data);

% Get group indices such that the data of group 1 is 
% ungrouped_data(group_indices(1, 1): group_indices(1, 2), :, :)
cum_sum_sizes = cumsum(group_sizes);
group_indices = [[1; 1 + cum_sum_sizes(1:end-1)], cum_sum_sizes];

% Create an index array corresponding to subjects that we can shuffle.
subj_indices = 1:size(ungrouped_data, 1);

% Loop over randomnization runs.
effect_sizes = NaN(n_randomnizations, n_maps);
fprintf('Starting %d randomnizations with %d subjects, %d groups and %d maps...\n', n_randomnizations, n_subjects, n_groups, n_maps)
for i_run = 0:n_randomnizations
    if mod(i_run, 10) == 0
        fprintf('Run %d/%d...\n', i_run, n_randomnizations)
    end
    
    % Shuffle subject indices.
    if i_run == 0  % for debugging.
        subj_indices = 1:n_subjects;
    else
        subj_indices = randperm(n_subjects);
    end
    
    % Loop over groups.
    all_mean_maps = nan(n_groups, n_maps, n_channels);
    for i_group = 1:n_groups
        % Create random group.
        group_i_indices = subj_indices(group_indices(i_group, 1): group_indices(i_group, 2));
        group_i_data = ungrouped_data(group_i_indices, :, :);
            
        % Find group mean MS maps using k-means clustering.
        maps_to_cluster = reshape(group_i_data, ...
            group_sizes(i_group)*n_maps, size(group_i_data, 3));  % To shape (subjects*maps x channels).
        [best_mean_map, b_ind, b_loading, ~] = eeg_kMeansCustom(maps_to_cluster, n_maps, restarts, [], flags);
            
%         figure('color', 'w');
%         plotClustering(maps_to_cluster, best_mean_map, b_ind, b_loading, ignore_polarity)
        
        all_mean_maps(i_group, :, :) = best_mean_map;
    end
    
    % Reshape to (n_channels, n_maps, n_groups).
    all_mean_maps = permute(all_mean_maps, [3, 2, 1]);

    % Compute generalized dissimilarity.
    [GD, MS_mean] = computeGeneralizedDissimilarity(all_mean_maps, [], ignore_polarity);
    
    % Save the value.
    if i_run == 0  % for debugging.
        MS_mean_true = MS_mean;
        GD_true = GD;
    else
        % Sort on MS_Mean_true. 
        GD_sorted = sortGD(GD, MS_mean_true, MS_mean, ignore_polarity);
        effect_sizes(i_run, :) = GD_sorted;
    end
end

fprintf('Done! ')
toc

mean(effect_sizes > GD_true')

function GD_sorted = sortGD(GD, MS_ref, MS_sort, ignore_polarity)

GD_sorted = nan(size(GD));

[n_channels, n_maps] = size(MS_ref);

GMD = computeDissimilarityIndex([MS_ref, MS_sort], ignore_polarity);

ref_vs_sort = GMD(1:n_maps, n_maps+1:end);

for ii = 1:n_maps
    idx = find(ref_vs_sort == min(ref_vs_sort(:)));
    [i_ref, i_sort] = ind2sub(size(ref_vs_sort), idx);
    GD_sorted(i_ref) = GD(i_sort);
    ref_vs_sort(i_ref, :) = inf;
    ref_vs_sort(:, i_sort) = inf;
end 

end
