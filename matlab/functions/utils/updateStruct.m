function s = updateStruct(s, s_new)
% Update the fields of a struct `s` with fields in `s_new`.
%
% Input: 
% - s : struct to update.
% - s_new : struct with new values to add/overwrite to `s`.
%
% Output:
% - s : updated struct.

fn = fieldnames(s_new);
for fi=1:numel(fn)
    s.(fn{fi}) = s_new.(fn{fi});
end
end