function letters = numbers2letters(numbers)
% Convert numbers to letters.
% 
% Example:
% >> numbers2letters([1, 2, 3, 4])
% {'A'} {'B'} {'C'} {'D'}

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
letters = cell(size(numbers));
for i = 1:length(numbers)
    letters{i} = alphabet(numbers(i));
end

end