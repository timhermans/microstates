function [GFP, MS_mean] = computeMeanGFP(MS_all, ignore_polarity)
% Compute GFP of the mean map.
% 
% Input: 
% - MS_all : matrix with MS maps to compare with shape 
% (n_channels, n_groups).
% - ignore_polarity : bool specifying whether to ignore polarity or not.
% Defaults to true.
%
% Output:
% - GD : vector with length n_maps containing the generalized dissimilarity
% for each map.

if nargin < 2 || isempty(ignore_polarity)
    ignore_polarity = true;
end

% Normalize.
MS_all = (MS_all - mean(MS_all, 1))./std(MS_all, 1, 1);

if ignore_polarity
    % Match polarity.
    for ii = 2:size(MS_all, 2)
        MS_i = MS_all(:, ii);
        MS_mean = mean(MS_all(:, 1:ii-1), 2);
        MS_i = matchPolarity(MS_i, MS_mean);
        MS_all(:, ii) = MS_i;
    end
end

% Compute mean.
MS_mean = mean(MS_all, 2);

% Compute GFP of mean map.
GFP = sqrt(mean((MS_mean - mean(MS_mean)).^2));

end