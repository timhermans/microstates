function plotClustering(MapsToCluster, BestMeanMap, b_ind, b_loading, IgnorePolarity)
% Helper function used in findGroupMSTemplates to visualize clustering.

% Reduce dimensions to 2 using PCA.
[coeff, score, latent] = pca(MapsToCluster);
means = mean(MapsToCluster);
X = (MapsToCluster - means)*coeff(:, 1:2);
Xmean = (BestMeanMap - means)*coeff(:, 1:2);

hold on
cols = lines;
n = size(BestMeanMap, 1);
for k = 1:n
    % Cluster centers.
    load = mean(b_loading(b_ind == k));
    center = Xmean(k, :)*load;
    scatter(center(1), center(2), [], cols(k, :), 'filled')
    
    if IgnorePolarity
        % Plot negative map.
        scatter(-center(1), -center(2), [], cols(k, :), 'filled')
    end

    % Original data points.
    scatter(X(b_ind == k, 1), X(b_ind == k, 2), [], cols(k, :))
end
xlabel('PC1')
ylabel('PC2')
title(sprintf('%d MS templates', n))
end