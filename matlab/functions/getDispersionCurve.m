function [k_all, W_all] = getDispersionCurve(ALLEEG, CURRENTSET)
% Compute dispersion W for each k (numbe of microstates).
%
% Input:
% - ALLEEG : struct from EEGLAB containing the EEG to process.
% - CURRENTSET : index pointing to the EEG in ALLEEG which to process.
%
% Output:
% - k_all : number of microstates for which W is computed.
% - W_all : the dispersion for each k.
msinfo = ALLEEG(CURRENTSET).msinfo;
k_min = msinfo.ClustPar.MinClasses;
k_max = msinfo.ClustPar.MaxClasses;
k_all = k_min : k_max;
W_all = NaN(length(k_all), 1);
data = ALLEEG(CURRENTSET).data;
for ik = 1:length(k_all)
    k = k_all(ik);
    % Compution quality measure W (dispersion). W trends toward 0 as the quality of the clustering results increases
    data_i = data(:, msinfo.L_all{k,1}~=0);
    tempL = msinfo.L_all{k,1}(msinfo.L_all{k,1}~=0);
    w = CalcDispersion(data_i, msinfo.A_all{k,1}, tempL);
    W_all(ik) = w; % Save dispersion for each k (number of kmeans clusters).
end

end
