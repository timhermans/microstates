function plotShaded(x, y_min, y_max, color, alpha)
% Plot shaded region between y_min and y_max for range of x.

if nargin < 5 || isempty(alpha)
    alpha = 0.5;
end
if nargin < 4 || isempty(color)
    color = 'k';
end
x = x(:)';
y_min = y_min(:)';
y_max = y_max(:)';
x2 = [x, fliplr(x)];
inBetween = [y_max, fliplr(y_min)];
fill(x2, inBetween, color, 'FaceAlpha', alpha);

end