function p_value = computeTANOVA2(maps, n_randomnizations, ignore_polarity, seed)
% Compute p value for comparing topographies with null hypothesis that the
% topographies are random (permutation test).
%
% Input: 
% - maps : matrix with MS maps to compare with shape 
% (n_channels, n_groups).
% - n_randomnizations : number of randomnizations.
% - ignore_polarity : bool specifying whether to ignore polarity or not.
% Defaults to true.
% - seed : intiger to seed the random generator (for reproducability). If
% empty, no seed is applied.

if nargin < 2 || isempty(n_randomnizations); n_randomnizations = 5000; end
if nargin < 3 || isempty(ignore_polarity); ignore_polarity = true; end
if nargin < 4; seed = 43; end

% Seed random generator.
if ~isempty(seed)
    rng(seed);
end

% Get sizes.
[n_channels, n_groups] = size(maps);

% Compute real effect size.
effect_size = computeEffectSize(maps, ignore_polarity);

% Compute randomizations.
effect_sizes_rand = nan(n_randomnizations, 1);
for i_rand = 1:n_randomnizations
    % Shuffle channels per group.
    maps_rand = nan(size(maps));
    for i_group = 1:n_groups
        rand_idx = randperm(n_channels);
        maps_rand(:, i_group) = maps(rand_idx, i_group);
    end
    
    % Compute effect size.
    effect_sizes_rand(i_rand) = computeEffectSize(...
        maps_rand, ignore_polarity); 
end

p_value = mean(effect_sizes_rand >= effect_size);

end


function es = computeEffectSize(MS_all, ignore_polarity)
es = computeMeanGFP(MS_all, ignore_polarity);

% % Normalize.
% MS_all = (MS_all - mean(MS_all, 1))./std(MS_all, 1, 1);
% 
% if ignore_polarity
%     % Match polarity.
%     for ii = 2:size(MS_all, 2)
%         MS_i = MS_all(:, ii);
%         MS_mean = mean(MS_all(:, 1:ii-1), 2);
%         MS_i = matchPolarity(MS_i, MS_mean);
%         MS_all(:, ii) = MS_i;
%     end
% end
% 
% % Compute mean.
% MS_mean = mean(MS_all, 2);
% 
% es = -sum(sqrt(mean((MS_all - MS_mean).^2, 1)));

end