function [EEG] = reorder_channels(EEG, channel_labels)

% Reorder channels.

% Get current labels.
chanlocs = EEG.chanlocs;
channel_labels_current = cell(length(chanlocs), 1);
for i = 1:length(chanlocs)
    label = chanlocs(i).labels;
    channel_labels_current{i} = label;
end

% Find indices of requested labes in the current labels.
reorder_idx = NaN(length(channel_labels), 1);
new_chanlocs = chanlocs(1:0);
for i = 1:length(channel_labels)
    channel = channel_labels{i};
    idx = find(strcmp(channel_labels_current, channel));
    if isempty(idx)
        error('Requested channel %s not in EEG dataset with channels [%s].', channel, strjoin(channel_labels_current, ','))
    end
    reorder_idx(i) = idx;
    new_chanlocs(i) = chanlocs(idx);
end

% Reorder data.
EEG.data = EEG.data(reorder_idx, :);

end