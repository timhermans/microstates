function paths = getPaths()

% The paths variable will contain all relevant paths.
paths = struct;

% Automatically detect root dir. The root dir should contain the folders
% 'functions', and 'channel_locations'. 
% In case this automatic detection doesn't work on your system, you can 
% hardcode the root dir in the corresponding if-clause below.
this_dir = fileparts(mfilename('fullpath'));
paths.root_dir = this_dir(1: 1 + end - strfind(reverse(this_dir), reverse('microstates')));

if contains(lower(this_dir), '/users/sista/thermans/')
    %%%%%%%%%%%%%%%%%%%%
    % On server.
    %%%%%%%%%%%%%%%%%%%%
    paths.eeglab_dir = '/users/sista/thermans/packages/eeglab2021.0';
    paths.recording_data_file = '/users/sista/thermans/data/metadata/microstates/recording_data.xlsx';
    paths.output_root_dir = '/esat/biomeddata/thermans/cache/microstates';
    paths.D1_data_dir = '/esat/biomeddata/Mario/ninah_data/';
    paths.D2_data_dir = '/esat/stadiusdata/sensitive/Neonatal-preterm-maturation/neonatal-preterm-maturation-43/';
    
elseif contains(lower(this_dir), 'c:\users\thermans')
    %%%%%%%%%%%%%%%%%%%%
    % On PC thermans.
    %%%%%%%%%%%%%%%%%%%%
    paths.eeglab_dir = 'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\my_code\packages\matlab\eeglab\eeglab2021.0';
    paths.recording_data_file = 'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\metadata\microstates\recording_data.xlsx';
    paths.output_root_dir = 'D:\data\processed\microstates';
    paths.D1_data_dir = 'W:\sensitive\Leuven_pretermEEG\Leuven_pretermEEG';
    paths.D2_data_dir = 'D:\data\raw\Neonatal-preterm-maturation\neonatal-preterm-maturation-43';
else
    error('Unknown platform. Cannot set paths. Please add your platform and paths to this function.')
end

paths.chanlocs_file = fullfile(paths.root_dir, 'channel_locations', 'Channel_Location_9electrodes.ced');
paths.functions_dir = fullfile(paths.root_dir, 'functions');

end