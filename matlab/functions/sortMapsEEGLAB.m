function [sorted_maps, mean_map] = sortMapsEEGLAB(all_maps, ignore_polarity)
% Sort the microstate maps using EEGLAB plugin code (but without using
% EEGLAB).
%
% Input: 
% - all_maps : matrix with MS maps to sort with shape 
% (n_channels, n_maps, n_groups).
% - ignore_polarity : bool specifying whether to ignore polarity or not.
% Defaults to true.
%
% Output:
% - sorted_maps : sorted maps (same shape as input).
% - mean_map : optimal mean map with shape (n_channels, n_maps).

if nargin < 2 || isempty(ignore_polarity); ignore_polarity = true; end

% Do the sorting.
[mean_map, sorted_maps, ~] = PermutedMeanMapsCustom(permute(all_maps, [3 2 1]), ~ignore_polarity);

% Change shapes of matrices.
sorted_maps = permute(sorted_maps, [3 2 1]);
mean_map = mean_map';

end
