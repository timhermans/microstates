function [k, count] = countOptimalK(filepaths, include_in_count, verbose)
% Count the number of files that have optimal k according to KL.
%
% Input:
% - filepaths (cell): cell array with all filepaths to .set files to
% include.
% - include_in_count (array): array of ranks (integers) that define which k
% are selected as optimal. E.g. if include_in_count is [1], then only the
% k at maximum KL is counted. Or, if [1, 2], the maximum and the second
% highest KL are counted. Or, if [2], the second highest KL are counted.
% - verbose (int): verbosity level.
%
% Output:
% - k (array): array with k (number of MS maps).
% - count (array): array with same size as k that contains the number of
% recordings (files) that had an optimal k, as function of k.
if nargin < 3 || isempty(verbose)
    verbose = 0;
end

if nargin < 2 || isempty(include_in_count)
    include_in_count = [1];
end

% Loop over patients and process.
N = numel(filepaths);
count = zeros(30, 1);
k_max = 0;
k_min = 100;
for i_file = 1:N
    if verbose
        fprintf('%i/%i\n', i_rec, N);
    end

    filepath_set = filepaths{i_file};
    if ~isfile(filepath_set)
        error('File %s does not exist.', filepath_set)
    end
        
    % Only load the MS info (much faster than loading the entire file).
    load('-mat', filepath_set, 'msinfo')

    % Read k and corresponding KL.
    k_i = (msinfo.ClustPar.MinClasses + 1): (msinfo.ClustPar.MaxClasses - 1); % KL is not computed for min and max k.
    KL_i = cell2mat(msinfo.KL);   
    k_min_i = min(k_i);
    if k_min_i < k_min
        k_min = k_min_i;
    end
    
    k_max_i = max(k_i);
    if k_max_i > k_max
        k_max = k_max_i;
    end
    
    % Highest ones.
    [~, idx_sort] = sort(KL_i, 'descend');
    k_optimal_i = k_i(idx_sort(include_in_count));
    
    % Update the count.
    count(k_optimal_i) = count(k_optimal_i) + 1;
end

count = count(k_min:k_max);
k = k_min:k_max;

end


