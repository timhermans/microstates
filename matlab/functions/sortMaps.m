function sorted_maps_global = sortMaps(all_maps, restarts, verbose, metric)
% Sort the microstate maps such that the average dissimilarity between the
% maps of all groups is as low as possible. Also matches the polarity.
%
% Input: 
% - all_maps : matrix with MS maps to sort with shape 
% (n_channels, n_maps, n_groups).
% - restarts : number of restarts with different order of permuting the
% groups. if empty, tries all possible restarts. For larger amount of groups
% this can be slow.
% - metric : string specifying which metric to optimize for sorting. Choose
% from 'dissimilarity' or 'GFP'. 
% - verbose : intiger specifying the level of verbose (print messages).
%
% Output:
% - sorted_maps_global : sorted maps (same shape as input).

if nargin < 2; restarts = 30; end
if nargin < 3 || isempty(metric); metric = 'dissimilarity'; end
if nargin < 4 || isempty(verbose); verbose = 1; end
max_iter = 10;

[n_channels, n_maps, n_groups] = size(all_maps);

% All possible orders for permuting groups.
perm_orders = perms(1:n_groups);

if ~isempty(restarts) && (restarts < size(perm_orders, 1))
    % Limit the number of restarts.
    rng(43)  % Seed random generator for reproducibility.
    rand_idx = randperm(size(perm_orders, 1));
    perm_orders = perm_orders(rand_idx(1:restarts), :);
end

% All possible permutations of maps.
perm_maps = perms(1:n_maps);

% Init.
init_maps = all_maps*1;
sorted_maps_global = all_maps*1;
diss_global = computeMetric(sorted_maps_global, metric);

if verbose == 1
    fprintf('Sorting %d maps in %d groups...\n', n_maps, n_groups)
end

% It may be different depending which group we optimize first, so we do it
% for a number of different orders in which we optimize.
for i_order = 1:length(perm_orders)
    order = perm_orders(i_order, :);
    
    % Permute again untill no better option is found.
    n_iter = 0;
    sort_maps = init_maps*1;    
    diss_old = inf;
    diss = computeMetric(sort_maps, metric);
    while (diss < diss_old) && (n_iter < max_iter)
        diss_old = diss*1;
        
        % Loop over groups.
        for ii = 1:length(order)
            i_group = order(ii);

            % All possible permutations of maps in the current group.
            for i_perm = 1:size(perm_maps, 1)
                % New indices for permutation of maps.
                new_idx = perm_maps(i_perm, :);

                % Permute maps.
                new_maps = sort_maps*1;
                new_maps(:, :, i_group) = new_maps(:, new_idx, i_group);

                % Compute dissimilarity.
                diss_new = computeMetric(new_maps, metric);                    

                % If better, save.
                if diss_new < diss
                    diss = diss_new*1;
                    sort_maps = new_maps*1;
                end
                if diss_new < diss_global
                    diss_global = diss_new*1;
                    sorted_maps_global = new_maps*1;
                    if verbose > 2
                        fprintf('%f, ', diss_global)
                    end
                end
            end
        end
        n_iter = n_iter + 1;
    end
    if verbose > 1
        fprintf('Sorted maps after %d iterations.\n', n_iter)
        disp(diss_global)
    end
end

% Match polarity.
for i_map = 1:size(sorted_maps_global, 2)
    for i_group = 2:size(sorted_maps_global, 3)
        MS_i = sorted_maps_global(:, i_map, i_group);
        MS_mean = mean(sorted_maps_global(:, i_map, 1:i_group-1), 3);
        MS_i = matchPolarity(MS_i, MS_mean);
        sorted_maps_global(:, i_map, i_group) = MS_i;
    end
end

end

function diss = computeMetric(maps, metric)
% Helper function to compute dissimilarity (this value will be minimized).
ignore_polarity = true;
if strcmpi(metric, 'dissimilarity')
    diss = computeMeanDissimilarity(maps);
elseif strcmpi(metric, 'gfp')
    % Loop over maps.
    diss = nan(size(maps, 2), 1);
    for ii = 1:size(maps, 2)
        diss(ii) = computeMeanGFP(squeeze(maps(:, ii, :)), ignore_polarity);
    end
    % GFP is a measure for similarity, so take the negative so that if we
    % minimize it, we maximize similarity.
    diss = -mean(diss);
else
    error('Invalid metric for sorting "%s". Choose `dissimilarity` or `gfp`.', metric)
end
end
