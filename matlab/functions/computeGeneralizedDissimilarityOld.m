function [GD, MS_mean] = computeGeneralizedDissimilarityOld(MS_all, MS_mean, ignore_polarity)
% Compute Generalized Dissimilarity per MS map.
% Eq. 1, Chapter 8, in Electrical Neuroimaging by Michel, Koenig, et al.
% 
% Input: 
% - MS_all : matrix with MS maps to compare with shape 
% (n_channels, n_maps, n_groups).
% - MS_mean : mean map to use with shape (n_channels, n_maps).
% - ignore_polarity : bool specifying whether to ignore polarity or not.
% Defaults to true.
%
% Output:
% - GD : vector with length n_maps containing the generalized dissimilarity
% for each map.

if nargin < 3 || isempty(ignore_polarity)
    ignore_polarity = true;
end

if nargin < 2 || isempty(MS_mean)
    % Sort and find mean if not given.
    [MS_all, MS_mean] = sortMaps(MS_all, ignore_polarity);
end

% Check sizes.
[n_channels, n_maps, n_groups] = size(MS_all);
if size(MS_mean, 1) ~= n_channels || size(MS_mean, 2) ~= n_maps
    error('Sizes of MS_all and MS_mean are not compatible. Sizes should be (n_channels, n_maps, n_groups) for MS_all and (n_channels, n_maps) for MS_mean.')
end

% Compute GD for each map, which is simply the sum of root mean squared
% differences over the conditions.
RMSD = squeeze(sqrt(mean((MS_all - MS_mean).^2, 1)));
if ignore_polarity
    RMSD_2 = squeeze(sqrt(mean((-MS_all - MS_mean).^2, 1)));
    mask = RMSD_2 < RMSD;
    RMSD(mask) = RMSD_2(mask);
end
GD = sum(RMSD, 2);

end