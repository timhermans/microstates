function EEG_set = loadEEGSet(filepath, channels)
% Load a mat file and create an EEG set (EEGLAB).
%
% Input:
% - filepath (str): filepath to .mat file (see code which variables this
% .mat file must contain).
% - channels (cell): cell array with channel labels to load. The returned
% EEG set struct will contain the data in the order of these specified
% labels.
%
% Output:
% - EEG_set (struct): EEG set for EEGLAB.

load(filepath, 'EEG', 'channel_labels', 'fs', 'info', 'time_offset', 'unit')
[~, filename, ext] = fileparts(filepath);

% Channel labels to cell array.
channel_labels_new = cell(size(channel_labels, 1), 1);
for ii = 1:length(channel_labels)
    channel_labels_new{ii} = strip(replace(channel_labels(ii, :), 'EEG', ''));
end
channel_labels = channel_labels_new;

% Check dimensions.
if size(EEG, 1) > size(EEG, 2)
    EEG = EEG';
end

% By default load all channels.
if nargin < 2 || isempty(channels)
    channels = channel_labels;
end

% Reorder EEG according to `channels`.
EEG_new = NaN(length(channels), size(EEG, 2));
channel_labels_new = cell(length(channels), 1);
for ii = 1:length(channels)
    chan = channels{ii};
    idx = find(strcmp(chan, channel_labels));
    EEG_new(ii, :) = EEG(idx, :);
    channel_labels_new{ii} = channel_labels{idx};
end
EEG = EEG_new;
channel_labels = channel_labels_new;

% Create chanlocs struct.
chanlocs = struct;
for ii = 1:length(channel_labels)
    chanlocs(ii).labels = channel_labels{ii};
end

% Collect all data in an EEG set for EEGLAB.
EEG_set = struct;
EEG_set.data = EEG;
EEG_set.setname = filename;
EEG_set.filename = [filename, ext];
EEG_set.filepath = filepath;
EEG_set.comments = info;
EEG_set.nbchan = size(EEG, 1);
EEG_set.trials = 1;
EEG_set.pnts = size(EEG, 2);
EEG_set.srate = fs;
EEG_set.times = (1: size(EEG, 2))/fs/1000 + time_offset/1000;
EEG_set.chanlocs = chanlocs;
EEG_set.icawinv = [];
EEG_set.icaweights = [];
EEG_set.icasphere = [];
EEG_set.icaact = [];
EEG_set.xmax = (EEG_set.pnts - 1) / EEG_set.srate;
EEG_set.xmin = 0;
EEG_set = eeg_checkset(EEG_set);

end