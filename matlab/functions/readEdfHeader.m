function [hdr] = readEdfHeader(fid)
%readEdfHeader - Reads the header of a European Data Format (EDF) file.
%Reads the header of a European Data Format (EDF) file and collects the 
%information of the EDF header in a struct with fields corresponding to 
%the header's content.
%
%Adopted from Brett Shoelson:
%https://nl.mathworks.com/matlabcentral/fileexchange/31900-edfread
%
%See https://www.edfplus.info/specs/edf.html for header structure.
%
% Syntax: hdr = readHdfHeader(fid)
%
% Inputs:
%   fid - file ID of the open binary EDF file, e.g. fid = fopen(fname).
%
% Outputs:
%   hdr - struct containing (only) the information in the EDF header.
%
% Example:
%   % Open an EDF file
%   fid = fopen('fname.edf');
%   % Read header
%   hdr = readEdfHeader(fid);
%
% See also: findTargetSignalsIndices, readEdfData

% Written by Tim Hermans April 29, 2019
% tim-hermans@hotmail.com

% Return to the beginning of the file.
frewind(fid)

% Specify the precision for reading the bytes.
precision = 'uint8';

% 8 ascii : version of this data format (0) 
hdr.ver        = str2double(char(fread(fid, 8, precision)'));
% 80 ascii : local patient identification (mind item 3 of the additional EDF+ specs)
hdr.patientID  = char(fread(fid,80,precision)');
% 80 ascii : local recording identification (mind item 4 of the additional EDF+ specs)
hdr.recordID   = char(fread(fid,80,precision)');
% 8 ascii : startdate of recording (dd.mm.yy) (mind item 2 of the additional EDF+ specs)
hdr.startdate  = char(fread(fid,8,precision)');% (dd.mm.yy)
% 8 ascii : starttime of recording (hh.mm.ss) 
hdr.starttime  = char(fread(fid,8,precision)');% (hh.mm.ss)
% 8 ascii : number of bytes in header record 
hdr.hdrSizeInBytes = str2double(char(fread(fid,8,precision)'));
% 44 ascii : reserved
hdr.reserved1  = char(fread(fid,44)');
% 8 ascii : number of data records (-1 if unknown, obey item 10 of the additional EDF+ specs) 
hdr.numRecords = str2double(char(fread(fid,8,precision)'));
% 8 ascii : duration of a data record, in seconds 
hdr.durationOfRecord   = str2double(char(fread(fid,8,precision)'));
% 4 ascii : number of signals (ns) in data record 
hdr.numSignals = str2double(char(fread(fid,4,precision)'));

% ns * 16 ascii : ns * label (e.g. EEG Fpz-Cz or Body temp) (mind item 9 of the additional EDF+ specs)
for ii = 1:hdr.numSignals
    hdr.label{ii} = char(fread(fid, 16, precision)');
end

% ns * 80 ascii : ns * transducer type (e.g. AgAgCl electrode) 
for ii = 1:hdr.numSignals
    hdr.transducer{ii} = char(fread(fid,80,precision)');
end

% ns * 8 ascii : ns * physical dimension (e.g. uV or degreeC) 
for ii = 1:hdr.numSignals
    hdr.units{ii} = char(fread(fid,8,precision)');
end

% ns * 8 ascii : ns * physical minimum (e.g. -500 or 34) 
for ii = 1:hdr.numSignals
    hdr.physicalMin(ii) = str2double(char(fread(fid,8,precision)'));
end

% ns * 8 ascii : ns * physical maximum (e.g. 500 or 40) 
for ii = 1:hdr.numSignals
    hdr.physicalMax(ii) = str2double(char(fread(fid,8,precision)'));
end

% ns * 8 ascii : ns * digital minimum (e.g. -2048) 
for ii = 1:hdr.numSignals
    hdr.digitalMin(ii) = str2double(char(fread(fid,8,precision)'));
end

% ns * 8 ascii : ns * digital maximum (e.g. 2047) 
for ii = 1:hdr.numSignals
    hdr.digitalMax(ii) = str2double(char(fread(fid,8,precision)'));
end

% ns * 80 ascii : ns * prefiltering (e.g. HP:0.1Hz LP:75Hz) 
for ii = 1:hdr.numSignals
    hdr.prefilter{ii} = char(fread(fid,80,precision)');
end

% ns * 8 ascii : ns * nr of samples in each data record 
for ii = 1:hdr.numSignals
    hdr.numSamplesInRecord(ii) = str2double(char(fread(fid,8,precision)'));
end

% ns * 32 ascii : ns * reserved (not saved)
for ii = 1:hdr.numSignals
    hdr.reserved2{ii} = char(fread(fid,32,precision)');
end

% Remove white spaces in relevant fields. 
hdr.label = strtrim(hdr.label);
hdr.transducer = strtrim(hdr.transducer);
hdr.units = strtrim(hdr.units);  

end