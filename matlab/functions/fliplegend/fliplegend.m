function []=fliplegend( labels )
l=legend(labels(end:-1:1));
legholder=l.String;
for i=1:length(legholder)
    l.String{i}=legholder{end+1-i};
end
end