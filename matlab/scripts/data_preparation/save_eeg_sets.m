% This script loops over all preprocessed .mat files, reads them and saves
% them as .SET files.
clear 
close all

%% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Where to load and save the files.
output_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs');

% Get chanlocs filepath.
chanlocs_file = paths.chanlocs_file;

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
[ALLEEG EEG CURRENTSET] = eeglab('nogui');

%% Process.
% List all .mat files.
all_filepaths = dir(fullfile(output_dir, '*.mat'));

% Channels to read.
chanlocs = readlocs(chanlocs_file);
channels = cell(length(chanlocs), 1);
for ii = 1:length(chanlocs)
    channels{ii} = chanlocs(ii).labels;
end

% Loop over .mat files.
for ii = 1:length(all_filepaths)
    filepath = fullfile(all_filepaths(ii).folder, all_filepaths(ii).name);
    
    % Create output filepath.
    [~, filename, ~] = fileparts(filepath);
    filepath_out = fullfile(output_dir, [filename, '.set']);
    
    % Skip if already processed.
    if ~isfile(filepath_out)
        % Load and set chanlocs.
        EEG = loadEEGSet(filepath, channels);
        EEG.chanlocs = pop_chanedit(EEG.chanlocs, 'load', {chanlocs_file});
        
        % Save as .SET.
        pop_saveset(EEG, ...
            'filepath', filepath_out, ...
            'check', 'on', ...
            'savemode', 'onefile'); 
    end
end
    