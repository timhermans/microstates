% Create dummy data.
clear
clc
close all

%% Set options.
n_channels = 9;
n_groups = 4;
n_randomnizations = 5000;
ignore_polarity = true;
seed = 44;
insert_noise_group = true;

snr_all = 0:0.1:3;
n_plots = 5;

snr_plot = snr_all(round(linspace(1, length(snr_all), n_plots)));

%% Process.
rng(seed)
p_values_all = [];
signal_maps = rand(n_channels, 1);
noise_maps = rand(n_channels, n_groups);
figure('color', 'w', 'Position', [100, 100, 1100, 400]);
for i_snr = 1:length(snr_all)
    snr = snr_all(i_snr);

    % Create maps.
    maps = snr*signal_maps + noise_maps;
    if insert_noise_group
        maps(:, end-1) = noise_maps(:, end-1);
    end
    
    % Normalize.
    maps = (maps - mean(maps, 1))./std(maps, 1, 1);
    
    % Compute p values.
    p_values_all(i_snr) = computeTANOVA2(maps, n_randomnizations, ignore_polarity);
    
    % Plot.
    if any(snr == snr_plot)
        for i_map = 1:size(maps, 2)
            subplot(n_groups, length(snr_plot), find(snr == snr_plot) + (i_map - 1)*length(snr_plot))
            plotMap(maps(:, i_map))
            title(sprintf('SNR = %.1f', snr))
        end
    end
end

%% Plot.
% Plot snr vs pval.
figure('color', 'w', 'Position', [100, 100, 1100, 400]);
plot(snr_all, p_values_all, 'marker', 'o')
yline(0.05, '--')
xlabel('SNR (-)')
ylabel('p-value (-)')
title('Test case')