% EEGLAB history file generated on the 28-Apr-2023
% ------------------------------------------------

EEG.etc.eeglabvers = '2021.0'; % this tracks which version of EEGLAB is being used, you may ignore it
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG, 'time',[0 150] );
EEG.setname='PT 30_1_preprocessed_NQS_subseg';
EEG = eeg_checkset( EEG );
