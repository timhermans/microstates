% This script loops over all preprocessed .set files, performs MS analysis
% and saves it to new files.
clear 
close all

%% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))

%% Loop over batches. Note that you can run this loop in parallel if you want.
num_batches = 20;
for i_batch = 1:num_batches
    computeMSBatch(i_batch, num_batches)
end