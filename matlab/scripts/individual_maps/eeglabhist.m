% EEGLAB history file generated on the 28-Apr-2023
% ------------------------------------------------
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
EEG = pop_loadset('filename','PT 30_1_preprocessed_NQS.set','filepath','D:\\data\\processed\\microstates\\preprocessed\\epochs\\microstates\\group_level\\');
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG, 'time',[0 150] );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname','PT 30_1_preprocessed_NQS_subseg','gui','off'); 
eeglab redraw;
