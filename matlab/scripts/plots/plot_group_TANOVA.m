% This script loads the group level MS maps and p values for group TANOVA.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the directory with the group level maps.
grp_dir = fullfile(set_dir, 'group_level', 'sorted');

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
eeglab('nogui');

%% Options.
% Number of microstate maps (per group).
n_maps = 4;

% For p value.
n_permutations = 5000;

outcome = 'normal';

sleep_stages = {'QS', 'NQS'};

PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

sort_maps = false;  % If true, sorts maps before plotting. If false, does not.

% Figure position (size).
pos = [200 200 700 600];

%% Process.
all_PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
n_groups = length(PMA_groups);
n_rows = n_groups;
n_cols = n_maps;

% Loop over sleep stages.
p_values = table();
p_values.Sleep = replace(sleep_stages, 'NQS', 'AS')';
for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    figure('color', 'w', 'Position', pos)

    % Loop over groups.
    MS_all = [];  % n_chan x n_maps x n_groups
    for i_pma = 1:length(PMA_groups)
        pma_group = PMA_groups{i_pma};
        
        % Create filepath.
        filename = sprintf('GroupMean_%s_%d_%s.set', outcome, ...
            find(strcmp(pma_group, all_PMA_groups)) , ss);

        % Load EEG.
        EEG = pop_loadset(fullfile(grp_dir, filename));

        % Extract maps.
        MS = EEG.msinfo.MSMaps(n_maps).Maps'; % n_chan x n_maps
        
        % Save in matrix.
        MS_all = cat(3, MS_all, MS);
    end
    
    if sort_maps
        % Sort maps.
        MS_all = sortMaps(MS_all);
    end
    
    % Match polarity.
    for ii = 2:size(MS_all, 3)
        for kk = 1:n_maps
            MS_i = MS_all(:, kk, ii);
            MS_mean = mean(MS_all(:, kk, 1:ii-1), 3);
            MS_i = matchPolarity(MS_i, MS_mean);
            MS_all(:, kk, ii) = MS_i;
        end
    end

    % Loop over maps.
    for i_map = 1:n_maps
        pval = computeTANOVA2(squeeze(MS_all(:, i_map, :)), n_permutations);
        mapname = upper(char(96+i_map));  % A, B, C, ...
        p_values.(mapname)(i_ss) = pval;
        
        % Plot.
        for i_pma = 1:length(PMA_groups)
            subplot(n_rows, n_cols, i_map + (i_pma - 1)*n_maps)
            plotMap(MS_all(:, i_map, i_pma), ...
                EEG.chanlocs, EEG.msinfo.MSMaps(n_maps).ColorMap(i_map ,:)) 
            if i_map == 1
                ylabel(PMA_groups{i_pma}, 'Rotation', 0, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
            end
        end
        
        % Set title.
        subplot(n_rows, n_cols, i_map)
        title(sprintf('p = %.4f', pval))
    end 
    suptitle(ss)
end

disp(p_values)
