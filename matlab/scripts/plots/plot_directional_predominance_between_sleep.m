% This script loads the result_mean.csv file containing the MS results in
% long format (one recording per row) and plots significant differences in
% directional predominance between sleep stages within the same age group.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the result_mean.csv file (created by Python script after 
% MS metrics computation).
results_fp = 'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\results_mean.csv';

% What to plot.
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% Significance levels.
alpha_1 = 0.1;  % Grey.
alpha_2 = 0.05;  % Black.

%% Load.
% Load results.
results = readtable(results_fp);

%% Test.
% if true
%     disp('!In test mode, do not use these results!')
%     mask = strcmp(results.SleepStage, 'QS') & strcmp(results.AgeGroup, '<=31');
%     results{mask, 'OrgTM_1__2'} = normrnd(0.1, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_2__1'} = normrnd(0.3, 0.05, sum(mask), 1);
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '<=31');
%     results{mask, 'OrgTM_1__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_2__1'} = normrnd(0.1, 0.05, sum(mask), 1);
%     
%     mask = strcmp(results.SleepStage, 'QS') & strcmp(results.AgeGroup, '32-33');
%     results{mask, 'OrgTM_3__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_2__3'} = normrnd(0.1, 0.05, sum(mask), 1);
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '32-33');
%     results{mask, 'OrgTM_3__2'} = normrnd(0.1, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_2__3'} = normrnd(0.3, 0.05, sum(mask), 1);
%     
%     mask = strcmp(results.SleepStage, 'QS') & strcmp(results.AgeGroup, '>=37');
%     results{mask, 'OrgTM_3__1'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_1__3'} = normrnd(0.1, 0.05, sum(mask), 1);
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '>=37');
%     results{mask, 'OrgTM_3__1'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'OrgTM_1__3'} = normrnd(0.1, 0.05, sum(mask), 1);
% end

%% Process.
% Find columns indices with transition probabilities.
columns = results.Properties.VariableNames;
idx_T = find(contains(columns, 'OrgTM'));
T_columns = columns(idx_T);
n_maps = sqrt(length(T_columns));

if n_maps ~= 4
    error('This code only works for 4 MS templates, but identified %d maps.', n_maps)
end

% Color limits (probability).
color_lims = round([min(min(results{:, idx_T})), max(max(results{:, idx_T}))*0.65], 2);

% Loop over sleep stages.
P = NaN(n_maps, n_maps, length(PMA_groups));
for i_pma = 1:length(PMA_groups)
    pma = PMA_groups{i_pma};
    
    % Extract results.
    results_pma =  results(strcmp(results.AgeGroup, pma), :);
    
    % Sleep labels.
    ss_a = sleep_stages{1};
    ss_b = sleep_stages{2};

    % Extract results.
    results_a =  results_pma(strcmp(results_pma.SleepStage, ss_a), :);
    results_b =  results_pma(strcmp(results_pma.SleepStage, ss_b), :);

    % Build difference matrix D and pvalues P.
    for x = 1:n_maps
        for y = 1:n_maps
            if y == x
                continue
            end

            % Extract probabilities X->Y and Y->X.
            label_x_y = sprintf('OrgTM_%d__%d', x, y);
            label_y_x = sprintf('OrgTM_%d__%d', y, x);
            P_x_y_a = results_a.(label_x_y);
            P_y_x_a = results_a.(label_y_x);
            P_x_y_b = results_b.(label_x_y);
            P_y_x_b = results_b.(label_y_x);

            % Difference in probabilities.
            D_a = P_x_y_a - P_y_x_a;
            D_b = P_x_y_b - P_y_x_b;

            % t-test to test difference in mean.
            [~, pval] = ttest2(D_a, D_b);

            P(x, y, i_pma) = pval;
        end
    end
end

%% Correct pvalues.
fprintf('Number of significant before correction: %d \n', sum(P(:) < alpha_2))
[~, ~, ~, P(~isnan(P))] = fdr_bh(P(~isnan(P)));
fprintf('Number of significant after correction : %d \n', sum(P(:) < alpha_2))

%% Plot.
% Init figure.
f = figure('Color', 'w', 'Position', 0.8*[365   765   875   213]);
pad = 0.2;
arrow_width = 2.5;

% Loop over age groups.
for i_pma = 1:length(PMA_groups)
    pma = PMA_groups{i_pma};
    P_i = P(:, :, i_pma);

    % Create adjanency matrix.
    A_i = zeros(size(P_i));
    for ii = 1:size(P_i, 1)
        for jj = 1:size(P_i, 2)
            if jj == ii
                continue
            end
            pi = P_i(ii, jj);
            if pi < alpha_1
                % Determine index in A_i.
                i_target = ii;
                j_target = jj;

                if pi < alpha_2
                    % Black.
                    value = 2;
                else
                    % Grey.
                    value = 1;
                end

                A_i(i_target, j_target) = value;
            end
        end
    end

    % Plot.
    subplot(1, length(PMA_groups), i_pma)

    % Write letters indicating MS. 
    for i = 1:n_maps
        label = numbers2letters(i);
        [x, y] = get_coords(i);
        text(x, y, label, 'fontweight', 'bold', 'HorizontalAlignment', 'center')

        % Box around it.                        
        rectangle(...
          'Position', [x-pad y-pad pad*2 pad*2],...
          'LineWidth',1,...
          'EdgeColor','k')
    end

    % Draw arrows.
    for i = 1:size(A_i, 1)
        for j = 1:size(A_i, 2)
            ai = A_i(i, j);
            if ai > 0
                % Determine color.
                if ai == 1
                    color = 0.6*ones(3, 1);
                elseif ai == 2
                    color = 0*ones(3, 1);
                else
                    error('Code should not reach here.')
                end
                % Draw arrow.
                [x_start, y_start] = get_coords(i);
                [x_end, y_end] = get_coords(j);
                anArrow = annotation('arrow', ...
                    'color', color, 'linewidth', arrow_width,...
                    'Headstyle', 'cback2');

                % Determine the start and end position of the arrow
                % by weighting the start and end coordinates.
                weights = [4, 1]/5;
                x_start_ar = weights * [x_start; x_end];
                y_start_ar = weights * [y_start; y_end];
                x_end_ar = weights * [x_end; x_start];
                y_end_ar = weights * [y_end; y_start];

                % Make position based on axes instead of figure coords
                % and set position.
                anArrow.Parent = gca; 
                anArrow.Position = [x_start_ar, y_start_ar, x_end_ar - x_start_ar, y_end_ar - y_start_ar];
            end
        end
    end

    title(sprintf('%s\n%s vs %s', pma, sleep_stages{1}, sleep_stages{2}))
    set(gca,'XColor', 'none','YColor','none')

    rectangle(...
      'Position', [0-pad 0-pad 1+pad*2 1+pad*2],...
      'LineWidth',1,...
      'EdgeColor','k')

    xlim([-pad, 1+pad])
    ylim([-pad, 1+pad])
    set(gca,'dataAspectRatio',[1 1 1])

end


function [x, y] = get_coords(i)
if i == 1
    % A
    x = 0;
    y = 1;
elseif i == 2
    % B
    x = 1;
    y = 1;
elseif i == 3
    % C
    x = 0;
    y = 0;
elseif i == 4
    x = 1;
    y = 0;
else
    error('Not implemented for i = %d', i)
end
end
