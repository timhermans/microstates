% This script loads the result_mean.csv file containing the MS results in
% long format (one recording per row) and plots transition matrices.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the result_mean.csv file (created by Python script after 
% MS metrics computation).
results_fp = 'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\results_mean.csv';

% What to plot.
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% What to plot. Choose from 'Observed', 'Expected', 'Relative'.
which = 'Relative';

% Significance levels (if which is 'Relative').
alpha_1 = 0.1;  % *
alpha_2 = 0.05;  % **

%% Load.
% Load results.
results = readtable(results_fp);

%% Test.
% if true
%     disp('!In test mode, do not use these results!')
%     mask = strcmp(results.SleepStage, 'QS') & strcmp(results.AgeGroup, '32-33');
%     results{mask, 'OrgTM_1__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_1__2'} = normrnd(0.3, 0.05, sum(mask), 1);
% 
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '34-36');
%     results{mask, 'OrgTM_3__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_3__2'} = normrnd(0.1, 0.05, sum(mask), 1);
% 
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '>=37');
%     results{mask, 'OrgTM_1__3'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_1__3'} = normrnd(0.1, 0.05, sum(mask), 1);
% end

%% Process.
% Find columns indices with transition probabilities.
columns = results.Properties.VariableNames;
idx_T = find(contains(columns, 'OrgTM'));
T_columns = columns(idx_T);
n_maps = sqrt(length(T_columns));

% Init figure.
f = figure('Color', 'w');

% Color limits (probability).
if ~strcmpi(which, 'Relative')
    color_lims = round([min(min(results{:, idx_T})), max(max(results{:, idx_T}))*0.65], 2);
else
    color_lims = [-0.2, 0.2];
end

% Loop over sleep stages.
P = NaN(n_maps, n_maps, length(sleep_stages), length(PMA_groups));
for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    
    % Extract results.
    results_ss =  results(strcmp(results.SleepStage, ss), :);
    
    % Loop over age groups.
    for i_pma = 1:length(PMA_groups)
        pma = PMA_groups{i_pma};
        
        % Extract results.
        results_i =  results_ss(strcmp(results_ss.AgeGroup, pma), :);
        
        T = NaN(n_maps);
        for x = 1:n_maps
            for y = 1:n_maps
                if y == x
                    continue
                end
        
                % Extract probabilities X->Y.
                label_org = sprintf('OrgTM_%d__%d', x, y);
                label_exp = sprintf('ExpTM_%d__%d', x, y);
                P_org = results_i.(label_org);
                P_exp = results_i.(label_exp);
                
                if strcmpi(which, 'Relative')
                    P_i = (P_org - P_exp)./P_exp;
                    
                    % Paired t-test (for difference between obs and exp).
                    [~, pval] = ttest(P_org, P_exp);
                    P(x, y, i_ss, i_pma) = pval;
                    
                elseif strcmpi(which, 'Observed')
                    P_i = P_org;
                elseif strcmpi(which, 'Expected')
                    P_i = P_exp;
                else
                    error('Invalid choice which="%s". Choose from "Observed", "Expected", "Relative".', which)
                end
                
                % Mean.
                T(x, y) = mean(P_i);
            end
        end
        
        T(isnan(T)) = 0;
        
        % Plot matrix.
        subplot(length(sleep_stages), length(PMA_groups), ...
            i_pma+(i_ss - 1)*length(PMA_groups))
        imagesc(T, color_lims)
        if strcmpi(which, 'Relative')
            % Use diverging colormap.
            colormap(redwhiteblue(color_lims(1), color_lims(2)));
        end
        title(sprintf('%s (%s)', pma, ss))
        set(gca,'dataAspectRatio',[1 1 1])
        xticks(1:size(T, 1))
        xticklabels(numbers2letters(1:size(T, 1)))
        yticks(1:size(T, 2))
        yticklabels(numbers2letters(1:size(T, 2)))
    end
end

if strcmpi(which, 'Relative')
    cbar_label = '(P_{obs} - P_{exp}) / P_{exp}';
else
    cbar_label = 'Probability';
end
suptit = sprintf('%s transition probabilities', which);

h = colorbar('south', 'Ticks', color_lims, 'Fontsize', 9);
set(h, 'Position', [.14 .48 .75 .03])
h.Label.String = cbar_label;
h.Label.VerticalAlignment = 'top';
suptitle(suptit)

%% Put asterisks where significant.
if strcmpi(which, 'Relative')
    
    % Correct pvalues.
    fprintf('Number of significant before correction: %d \n', sum(P(:) < alpha_2))
    [~, ~, ~, P(~isnan(P))] = fdr_bh(P(~isnan(P)));
    fprintf('Number of significant after correction : %d \n', sum(P(:) < alpha_2))

    
    for i_ss = 1:length(sleep_stages)
        ss = sleep_stages{i_ss};
        % Loop over age groups.
        for i_pma = 1:length(PMA_groups)
            pma = PMA_groups{i_pma};
            P_i = P(:, :, i_ss, i_pma);

            for x = 1:n_maps
                for y = 1:n_maps
                    if y == x
                        continue
                    end
                    pi = P_i(x, y);
                    if pi < alpha_2
                        % **.
                        string = '**';
                    elseif pi < alpha_1
                        % *.
                        string = '*';
                    else
                        continue
                    end
                    subplot(length(sleep_stages), length(PMA_groups), ...
                        i_pma+(i_ss - 1)*length(PMA_groups))
                    text(y, x, string, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
                end
            end
        end    
    end
end
    
    


