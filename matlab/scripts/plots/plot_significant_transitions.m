% This script loads the result_mean.csv file containing the MS results in
% long format (one recording per row) and plots significant transitions per
% group.
% Significance is determined by comparing the observed with the expected
% transition probabilities (paired t-test).

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the result_mean.csv file (created by Python script after 
% MS metrics computation).
results_fp = 'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\results_mean.csv';

% What to plot.
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% Significance levels.
alpha_1 = 0.1;  % Grey.
alpha_2 = 0.05;  % Black.

% Whether to use non-parametric test (true) or a t-test (false).
non_paramteric = false;

% Init figure.
setPlotDefaults()

%% Load.
% Load results.
results = readtable(results_fp);

%% Test.
% if true
%     disp('!In test mode, do not use these results!')
%     mask = strcmp(results.SleepStage, 'QS') & strcmp(results.AgeGroup, '32-33');
%     results{mask, 'OrgTM_1__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_1__2'} = normrnd(0.3, 0.05, sum(mask), 1);
% 
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '34-36');
%     results{mask, 'OrgTM_3__2'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_3__2'} = normrnd(0.1, 0.05, sum(mask), 1);
% 
%     mask = strcmp(results.SleepStage, 'NQS') & strcmp(results.AgeGroup, '>=37');
%     results{mask, 'OrgTM_1__3'} = normrnd(0.3, 0.05, sum(mask), 1);
%     results{mask, 'ExpTM_1__3'} = normrnd(0.3, 0.05, sum(mask), 1);
% end

%% Process.
% Find columns indices with transition probabilities.
columns = results.Properties.VariableNames;
idx_T = find(contains(columns, 'OrgTM'));
T_columns = columns(idx_T);
n_maps = sqrt(length(T_columns));

if n_maps ~= 4
    error('This code only works for 4 MS templates, but identified %d maps.', n_maps)
end

% Color limits (probability).
color_lims = round([min(min(results{:, idx_T})), max(max(results{:, idx_T}))*0.65], 2);

% Loop over sleep stages.
D = NaN(n_maps, n_maps, length(sleep_stages), length(PMA_groups));
P = NaN(n_maps, n_maps, length(sleep_stages), length(PMA_groups));
for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    
    % Extract results.
    results_ss =  results(strcmp(results.SleepStage, ss), :);
    
    % Loop over age groups.
    for i_pma = 1:length(PMA_groups)
        pma = PMA_groups{i_pma};
        
        % Extract results.
        results_i =  results_ss(strcmp(results_ss.AgeGroup, pma), :);
        
        % Build difference matrix D and pvalues P.
        for x = 1:n_maps
            for y = 1:n_maps
                if y == x
                    continue
                end
                
                % Extract probabilities X->Y and Y->X.
                label_a = sprintf('OrgTM_%d__%d', x, y);
                label_b = sprintf('ExpTM_%d__%d', x, y);
                P_a = results_i.(label_a);
                P_b = results_i.(label_b);
                
                % Difference in probabilities.
                d_a_b = P_a - P_b;
                
                if non_paramteric
                    pval = signrank(d_a_b);
                else
                    % t-test (for zero mean, i.e. no difference).
                    [~, pval] = ttest(d_a_b);
                end
                
                D(x, y, i_ss, i_pma) = mean(d_a_b);
                P(x, y, i_ss, i_pma) = pval;
            end
        end
    end
end

%% Correct pvalues.
fprintf('Number of significant before correction: %d \n', sum(P(:) < alpha_2))
[~, ~, ~, P(~isnan(P))] = fdr_bh(P(~isnan(P)));
fprintf('Number of significant after correction : %d \n', sum(P(:) < alpha_2))

%% Plot.
% Init figure.
f = figure('Color', 'w', 'Position', [365 558 875 420]);
pad = 0.2;
arrow_width = 2.5;

for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    % Loop over age groups.
    for i_pma = 1:length(PMA_groups)
        pma = PMA_groups{i_pma};
        P_i = P(:, :, i_ss, i_pma);
        D_i = D(:, :, i_ss, i_pma);
        
        % Create adjanency matrix.
        A_i = zeros(size(P_i));
        for ii = 1:size(P_i, 1)
            for jj = 1:size(P_i, 2)
                if jj == ii
                    continue
                end
                pi = P_i(ii, jj);
                di = D_i(ii, jj);
                if pi < alpha_1
                    % Determine index in A_i.
                    i_target = ii;
                    j_target = jj;
                    
                    if pi < alpha_2
                        % Black.
                        value = 2;
                    else
                        % Grey.
                        value = 1;
                    end
                    
                    A_i(i_target, j_target) = value;
                end
            end
        end
        
        % Plot.
        subplot(length(sleep_stages), length(PMA_groups), ...
            i_pma+(i_ss - 1)*length(PMA_groups))
        
        % Write letters indicating MS. 
        for i = 1:n_maps
            label = sprintf('M%d', i);
            [x, y] = get_coords(i);
            text(x, y, label, 'fontweight', 'bold', 'HorizontalAlignment', 'center')
            
            % Box around it.                        
            rectangle(...
              'Position', [x-pad y-pad pad*2 pad*2],...
              'LineWidth',1,...
              'EdgeColor','k')
        end
        
        % Draw arrows.
        for i = 1:size(A_i, 1)
            for j = 1:size(A_i, 2)
                ai = A_i(i, j);
                di = D_i(i, j);
                if ai > 0
                    % Determine color.
                    if ai == 1
                        color = 0.6*ones(3, 1);
                    elseif ai == 2
                        color = 0*ones(3, 1);
                    else
                        error('Code should not reach here.')
                    end
                    
                    % Determine arrowstyle.
                    if di > 0
                        headstyle = 'cback2';
                    else
                        headstyle = 'rectangle';
                    end
                    
                    % Draw arrow.
                    [x_start, y_start] = get_coords(i);
                    [x_end, y_end] = get_coords(j);
                    anArrow = annotation('arrow', ...
                        'color', color, 'linewidth', arrow_width,...
                        'Headstyle', headstyle);
                    
                    % Determine the start and end position of the arrow
                    % by weighting the start and end coordinates.
                    weights = [4, 1]/5;
                    x_start_ar = weights * [x_start; x_end];
                    y_start_ar = weights * [y_start; y_end];
                    x_end_ar = weights * [x_end; x_start];
                    y_end_ar = weights * [y_end; y_start];
                    
                    % Make position based on axes instead of figure coords
                    % and set position.
                    anArrow.Parent = gca; 
                    anArrow.Position = [x_start_ar, y_start_ar, x_end_ar - x_start_ar, y_end_ar - y_start_ar];
                end
            end
        end
 
        title(sprintf('%s (%s)', pma, ss))
        set(gca,'XColor', 'none','YColor','none')
        
        rectangle(...
          'Position', [0-pad 0-pad 1+pad*2 1+pad*2],...
          'LineWidth',1,...
          'EdgeColor','k')

        xlim([-pad, 1+pad])
        ylim([-pad, 1+pad])
        set(gca,'dataAspectRatio',[1 1 1])
        
    end
end

function [x, y] = get_coords(i)
if i == 1
    % A
    x = 0;
    y = 1;
elseif i == 2
    % B
    x = 1;
    y = 1;
elseif i == 3
    % C
    x = 0;
    y = 0;
elseif i == 4
    x = 1;
    y = 0;
else
    error('Not implemented for i = %d', i)
end
end
