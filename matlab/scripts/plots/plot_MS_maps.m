% This script plots the group level MS maps.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the directory with the group level maps.
grp_dir = fullfile(set_dir, 'group_level', 'sorted');

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
eeglab('nogui');

%% Options.
% Number of microstate maps (per group).
n_maps = 4;

outcome = 'normal';

sleep_stages = {'QS', 'NQS'};

PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

sort_maps = false;  % If true, sorts maps before plotting. If false, does not.

% Whether to use topoplot or the old one.
use_topoplot = true;
map_limits = [-2.3, 2.3];

% Figure position (size).
pos = [200 200 700 600];
fontsize = 16;

%% Process.
all_PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
n_groups = length(PMA_groups);
n_rows = n_groups;
n_cols = n_maps;

% Loop over sleep stages.
p_values = table();
p_values.Sleep = replace(sleep_stages, 'NQS', 'AS')';
for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    figure('color', 'w', 'Position', pos)
    set(gcf,'defaultaxesfontsize', fontsize)

    % Loop over groups.
    MS_all = [];  % n_chan x n_maps x n_groups
    for i_pma = 1:length(PMA_groups)
        pma_group = PMA_groups{i_pma};
        
        % Create filepath.
        filename = sprintf('GroupMean_%s_%d_%s.set', outcome, ...
            find(strcmp(pma_group, all_PMA_groups)) , ss);

        % Load EEG.
        EEG = pop_loadset(fullfile(grp_dir, filename));

        % Extract maps.
        MS = EEG.msinfo.MSMaps(n_maps).Maps'; % n_chan x n_maps
        
        % Save in matrix.
        MS_all = cat(3, MS_all, MS);
    end
    
    if sort_maps
        % Sort maps.
        MS_all = sortMaps(MS_all);
    end
    
    % Match polarity.
    for ii = 2:size(MS_all, 3)
        for kk = 1:n_maps
            MS_i = MS_all(:, kk, ii);
            MS_mean = mean(MS_all(:, kk, 1:ii-1), 3);
            MS_i = matchPolarity(MS_i, MS_mean);
            MS_all(:, kk, ii) = MS_i;
        end
    end

    % Loop over maps.
    for i_map = 1:n_maps
        mapname = sprintf('M%d', i_map);  %upper(char(96+i_map));  % A, B, C, ...
        
        % Plot.
        for i_pma = 1:length(PMA_groups)
            subplot(n_rows, n_cols, i_map + (i_pma - 1)*n_maps)
                        
            plotMap(MS_all(:, i_map, i_pma), ...
                EEG.chanlocs, EEG.msinfo.MSMaps(n_maps).ColorMap(i_map ,:), use_topoplot, map_limits) 
            if i_map == 1
                ylabel(sprintf('%s  ', num2str(i_pma)), 'Rotation', 0, 'VerticalAlignment','middle', 'HorizontalAlignment','right', 'fontsize', fontsize)
            end
            
            % Adjust limits.
            xlim([-0.55, 0.55])
            ylimits = ylim();
            ylim([ylimits(1), 0.6])

        end
        % Set title.
        subplot(n_rows, n_cols, i_map)
        title(sprintf('%s', mapname), 'fontsize', fontsize)
        drawnow     
    end
    
    % Build suptitle.
    mysuptitle(ss, fontsize+2)
end

%%
setPlotDefaults()

fig = figure('color', 'w');
h = axes(fig,'visible','off'); 
c = colorbar(h,'Location', 'south', 'Position',[0.25 0.1 0.25 0.02]);  % attach colorbar to h
colormap(c,'jet')
caxis(h,map_limits);             % set colorbar limits
