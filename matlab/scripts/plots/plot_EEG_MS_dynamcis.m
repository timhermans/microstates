% This script loads a dataset and plots the EEG with the background color
% indicating the microstates.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to .set files. Either select the folder with individual microstates
% assigned or the group level microstates assigned.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', ...
    'microstates');

% Init figure.
setPlotDefaults()

%% Settings.
filename = 'PT 30_2_preprocessed_QS.set';

%% Load.
filepath_set = fullfile(set_dir, filename);
EEG = pop_loadset(filepath_set);    % Basic file read

%% Plot EEG.
spacing = 4;  % Space between channels. Scales with std.
color = 'k';
linewidth = 1;

% n_channels, n_time.
eeg = EEG.data;
t = (1:length(eeg))/EEG.srate;

std_eeg = mean(std(eeg, [], 2));

offset = 0;
channel_labels = {};
ytick_list = [];
figure('color', 'w');
hold on
for i = size(eeg, 1):-1:1
    x = eeg(i, :) + offset;
    chan_label = EEG.chanlocs(i).labels;
    channel_labels{end+1} = chan_label;
    ytick_list(end+1) = offset;
    offset = offset + std_eeg*spacing;
    plot(t, x, 'color', color, 'linewidth', linewidth)
end

yticks(ytick_list);
yticklabels(channel_labels);
ylim([ytick_list(1) - std_eeg*spacing, ytick_list(end) + std_eeg*spacing])
