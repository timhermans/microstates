% Plot KL stats to identify the optimal number of cluster (k).

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to .set files. Either select the folder with individual microstates
% assigned or the group level microstates assigned.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

%% Options.
outcome = 'normal'; 
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
verbose = 1;

%% Process.
% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
[ALLEEG EEG CURRENTSET] = eeglab('nogui');

% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Loop over PMA groups.
fig = figure('color', 'w', 'Position', [200 200 1000 400]);
for i_group = 1:length(PMA_groups)
    pma_group = PMA_groups{i_group};
    
    % Loop over sleep stages.
    for i_sleep = 1:length(sleep_stages)
        ss = sleep_stages{i_sleep};
        
        % Create cell array with all filepaths in the group.
        all_filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
          
        % Loop over patients and process.
        N = numel(all_filepaths);
        W_all = [];
        k_all = [];
        for i_file = 1:N
            if verbose
                fprintf('%i/%i\n', i_file, N);
            end

            filepath_set = all_filepaths{i_file};
            if ~isfile(filepath_set)
                error('File %s does not exist.', filepath_set)
            end

            EEG = pop_loadset(filepath_set);    % Basic file read
            [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 1, 'gui','off');        % And make this a new set
            ALLEEG = eeg_store(ALLEEG, EEG, CURRENTSET);  % Store the thing

            % Get k and W.
            [k_i, W_i] = getDispersionCurve(ALLEEG, CURRENTSET);
            if isempty(k_all)
                k_all = k_i;
            else
                assert(all(k_i == k_all))
            end
            
            % Update the count.
            W_all = [W_all, W_i];
        end    
        
        % Plot.
        subplot(length(sleep_stages), length(PMA_groups), i_group + (i_sleep - 1)*length(PMA_groups))
        mid = mean(W_all, 2);
        sd = std(W_all, [], 2);
        low = mid - sd;
        high = mid + sd;
        hold on
        plotShaded(k_all, low, high, 'k', 0.3);
        plot(k_all, mid, 'color', 'k', 'marker', 'o', 'MarkerFaceColor', 'k');

        title(sprintf('%s %s', pma_group, ss))
        if i_sleep == length(sleep_stages)
            xlabel('k (-)')
        end
        if i_group == 1
            ylabel('W (-)')
        end
    end
end
