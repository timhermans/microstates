% This script plots the GEV (global explained variance) as a function of the
% number of microstate maps (mean and standard deviation) for different PMA
% groups and for QS and NQS.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to .set files. Either select the folder with individual microstates
% assigned or the group level microstates assigned.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', ...
    'microstates', 'group_level');

%% Options.
outcome = 'normal';
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% If true, creates separate subplots for each group. Otherwise, only
% separate subplots for sleep stages.
separate_groups = true;

% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Init figure.
setPlotDefaults()

fig = figure('Position', [200 200 800 400]);
colors = lines;

%% Process.
% Collect all GEVs from all groups for k=4.
labels = {};
all_GEV_k4 = {};
% Loop over PMA groups.
for i_group = 1:length(PMA_groups)
    pma_group = PMA_groups{i_group};
    
    % Loop over sleep stages.
    for i_sleep = 1:length(sleep_stages)
        ss = sleep_stages{i_sleep};
        
        % Get all filepaths of the group and sleep stage.
        filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
        
        GEV = [];
        for i_file = 1:numel(filepaths)
            filepath_set = filepaths{i_file};

            % Only load the MS info (much faster than loading the entire file).
            load('-mat', filepath_set, 'msinfo')

            % Extract GEV (in percentage).
            GEV_i = msinfo.fit * 100;
            rerer
            GEV = [GEV, GEV_i];
        end
        
        k = msinfo.ClustPar.MinClasses: msinfo.ClustPar.MaxClasses;
        GEV_mean = mean(GEV(k, :), 2);
        GEV_std = std(GEV(k, :), [], 2);
        GEV_high = GEV_mean + GEV_std;
        GEV_low = GEV_mean - GEV_std;
                
        if separate_groups
            subplot(length(sleep_stages), length(PMA_groups), i_group + (i_sleep - 1)*length(PMA_groups))
        else
            subplot(length(sleep_stages), 1, i_sleep)
        end
        hold on
        color = colors(i_group, :);
        plot(k, GEV_mean, 'color', color, 'DisplayName', pma_group, 'marker', 'o')
        legend
        if separate_groups
            % Add shade.
            plotShaded(k, GEV_low, GEV_high, color, 0.3)
        end
        xlabel('k (-)')
        ylabel('GEV (%)')
        title(ss)
        
        all_GEV_k4{end + 1} = GEV(4, :);
        labels{end +1} = sprintf('%s %s', pma_group, ss);
        
    end
end

fprintf('Mean of all GEV at k=4: %f (%f)\n', mean(cell2mat(all_GEV_k4)), std(cell2mat(all_GEV_k4)));

%% Boxplot.
c = colormap(lines(length(PMA_groups)));

% Prepare matrix for boxplots.
sizes_all = [];
n_groups = length(all_GEV_k4);
for ii = 1:n_groups
    sizes_all(ii) = length(all_GEV_k4{ii});
end
max_size = max(sizes_all);

X = NaN(max_size, n_groups + 1);
labels_new = repmat({''}, n_groups+1, 1);
for ii = 1:n_groups
    splits = split(labels{ii}, ' ');
    pma_group = splits{1};
    ss = splits{2};
    
    % Find index where to insert.
    i_sleep = find(strcmp(ss, sleep_stages));
    i_pma = find(strcmp(pma_group, PMA_groups));
    idx = (i_sleep - 1)*(length(PMA_groups) + 1) + i_pma;
    
    xii = all_GEV_k4{ii};
    X(1:length(xii), idx) = xii;
%     labels_new{idx} = labels{ii};
end
C = [c; ones(1,3); c];  % this is the trick for coloring the boxes

% Plot.
figure('Color', 'w');
h = boxplot(X, ...'plotstyle', 'compact', ...
     'colors', C, 'labels', labels_new, 'symbol', ''); % label only two categories
hold on;
set(h, 'linewidth', 2) 

for ii = 1:4
    plot(NaN,1,'color', c(ii,:), 'LineWidth', 4);
end


xticks_new = [];
for ii = 1:length(sleep_stages)
    xticks_new(ii) = (length(PMA_groups) + 1)/2 + (ii - 1)*(length(PMA_groups) + 1);
end
xticks(xticks_new)
xticklabels(sleep_stages)

h = findobj(gca,'Tag','Box'); 
for j=1:length(h) 
    patch(get(h(j),'XData'), get(h(j),'YData'), C(end + 1 - j, :), 'FaceAlpha', .5); 
end 

legend(PMA_groups, 'location', 'se')
ylabel('GEV (%)')

