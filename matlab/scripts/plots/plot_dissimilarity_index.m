% This script loads the group level MS maps and computes the pairwise 
% dissimilarity between the maps and computes pvalues to test if 2 maps are
% significantly similar (indicated by asterisk in plot).

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the directory with the group level maps.
grp_dir = fullfile(set_dir, 'group_level', 'sorted');

% Add EEGLAB to path and start EEGLAB without GUI.
addpath(paths.eeglab_dir)
eeglab('nogui');

%% Options.
% Number of microstate maps (per group).
n_maps = 4;

% For p value.
n_permutations = 5000;

% If true, sorts maps before plotting. If false, does not.
sort_maps = false;  

%% QS vs AS per age group.
outcome = 'normal';
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% Init figure.
figure('color', 'w', 'Position', [200 200 1000 800])
nrows = round(sqrt(length(PMA_groups)));
ncols = ceil(sqrt(length(PMA_groups)));

% Loop over age groups.
for i_pma = 1:length(PMA_groups)
    pma_group = PMA_groups{i_pma};
    
    % Load the mean group level MS.
    filename_QS = sprintf('GroupMean_%s_%d_%s.set', outcome, i_pma, 'QS');
    filename_AS = sprintf('GroupMean_%s_%d_%s.set', outcome, i_pma, 'NQS');
    EEG_QS = pop_loadset(fullfile(grp_dir, filename_QS));
    EEG_AS = pop_loadset(fullfile(grp_dir, filename_AS));

    % Extract maps.
    MS_QS = EEG_QS.msinfo.MSMaps(n_maps).Maps'; % n_chan x n_maps
    MS_AS = EEG_AS.msinfo.MSMaps(n_maps).Maps'; % n_chan x n_maps

    % Collect as n_chan x n_maps x n_groups.
    MS_all = cat(3, MS_AS, MS_QS);
    group_labels = {'NQS', 'QS'};
    
    % Plot.
    subplot(nrows, ncols, i_pma)
    [GDM, pvalues] = plotGDM(MS_all, group_labels, sort_maps, true, n_permutations);
    title([pma_group, ' wks'])    
end


%% PMA groups per sleep stage.
outcome = 'normal';
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
sleep_stages = {'QS', 'NQS'};

% Init figure.
figure('color', 'w', 'Position', [200 200 1000 350])
nrows = round(sqrt(length(sleep_stages)));
ncols = ceil(sqrt(length(sleep_stages)));

% Loop over sleep stages.
for i_ss = 1:length(sleep_stages)
    ss = sleep_stages{i_ss};
    
    % Load the mean group level MS.
    group_labels = {};
    MS_all = [];
    for i_pma = 1:length(PMA_groups)
        pma = PMA_groups{i_pma};
        filename_i = sprintf('GroupMean_%s_%d_%s.set', outcome, i_pma, ss);
        EEG_i = pop_loadset(fullfile(grp_dir, filename_i));

        % Extract maps.
        MS_i = EEG_i.msinfo.MSMaps(n_maps).Maps'; % n_chan x n_maps

        % Collect as n_chan x n_maps x n_groups.
        MS_all(:, :, i_pma) = MS_i;
        group_labels{i_pma} = num2str(i_pma);
    end
    
    % Plot.
    subplot(nrows, ncols, i_ss)
    add_colorbar = i_ss == length(sleep_stages);
    [GDM, pvalues] = plotGDM(MS_all, group_labels, sort_maps, true, n_permutations, add_colorbar);
    title(ss)    
end
