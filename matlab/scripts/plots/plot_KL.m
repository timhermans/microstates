% Plot KL stats to identify the optimal number of cluster (k).

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to .set files. Either select the folder with individual microstates
% assigned or the group level microstates assigned.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

%% Options.
outcome = 'normal'; 
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
include_in_count = [1];  % Ranks which to include in the count (e.g. 1 for maximum, [1, 2] for maximum and second highest). 

%% Process.
T_pd = readtable(paths.recording_data_file);

% Loop over PMA groups.
fig = figure('color', 'w', 'Position', [200 200 1000 400]);
for i_group = 1:length(PMA_groups)
    pma_group = PMA_groups{i_group};
    
    % Loop over sleep stages.
    for i_sleep = 1:length(sleep_stages)
        ss = sleep_stages{i_sleep};
        
        % Create cell array with all filepaths in the group.
        all_filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
          
        % Count number of recordings with optimal k.
        [k, count] = countOptimalK(all_filepaths, include_in_count);
        
        % Plot.
        subplot(length(sleep_stages), length(PMA_groups), i_group + (i_sleep - 1)*length(PMA_groups))
        bar(k, count);
        title(sprintf('%s %s', pma_group, ss))
        if i_sleep == length(sleep_stages)
            xlabel('k (-)')
        end
        if i_group == 1
            ylabel('count (-)')
        end
    end
end
