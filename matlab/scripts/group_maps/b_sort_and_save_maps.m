% This script sorts and saves the group level MS maps so that the order of
% MS is consistent among groups.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to .set files.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates', 'group_level');

% Output path.
output_dir = fullfile(set_dir, 'sorted');

%% Options.
outcome = 'normal';
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
num_maps = 4;
resort = [1, 4, 2, 3];  % Manually re-sort the maps (so that they are in common order).

%% Process.
% Start EEGLAB without GUI.
eeglab('nogui');

% Create output dir if it does not exist.
if ~isfolder(output_dir)
    mkdir(output_dir)
end

% Loop over PMA groups.
filepaths_all = {};
MS_all = zeros(0, 0, 0);  % (end+1) indexing does not work if you set MS_all = []; ! apparently size([], 3) is 1 instead of the expected zero, so end+1 the first time refers to (:, :, 2) instead of (:, :, 1)!
for i_group = 1:length(PMA_groups)
    pma_group = PMA_groups{i_group};
    
    % Loop over sleep stages.
    for i_sleep = 1:length(sleep_stages)
        ss = sleep_stages{i_sleep};
        
        % Load group level map.
        filename = sprintf('GroupMean_%s_%d_%s.set', outcome, i_group, ss);
        filepath = fullfile(set_dir, filename);
        EEG = pop_loadset(filepath);
        
        % Get MS maps.
        MS = EEG.msinfo.MSMaps(num_maps).Maps';
        MS_all(:, :, end+1) = MS;
        
        filepaths_all{end + 1} = filepath;
    end
end

%% Sort maps.
MS_all = sortMaps(MS_all);

if ~isempty(resort)
    MS_all = MS_all(:, resort, :);
end

%% Save.
for i_maps = 1:length(filepaths_all)
    filepath = filepaths_all{i_maps};
    MS = MS_all(:, :, i_maps);
    
    % Load group level map.
    EEG = pop_loadset(filepath);

    % Set MS maps.
    EEG.msinfo.MSMaps(num_maps).Maps = MS';

    % Save.
    [~, filename, ext] = fileparts(filepath);
    pop_saveset(EEG, 'filename', [filename, ext], 'filepath', output_dir, 'savemode', 'onefile');
end