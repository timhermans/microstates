% This script loads the MS analysis of the group level, and assigns
% the group-level maps to individual recordings to compute the KL 
% divergence criterion.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to the directory with the group level maps.
grp_dir = fullfile(set_dir, 'group_level');

%% Options.
outcomes = {'normal'};
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% Specify which group mean to use. Choose from:
% - 'outcome_sleepstage_PMAgroup'
mean_group = 'outcome_sleepstage_PMAgroup';

% Defining fitting parameters
FitPars.b = 30;                   % the window size, if labels are smoothed
FitPars.lambda = 1;               % the non-smoothness penalty factor
FitPars.PeakFit = true;           % true if fitting is only done on GFP peaks and the assignment is interpolated in between, false otherwise
FitPars.BControl = true;

%% Process.
% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Add EEGLAB to path.
addpath(paths.eeglab_dir)

% Start EEGLAB without GUI.
[ALLEEG EEG CURRENTSET] = eeglab('nogui');
            
% Loop over outcomes.
count = 0;
all_group_mean_index = [];
for i_outcome = 1:numel(outcomes)
    outcome = outcomes{i_outcome};
    
    % Loop over PMA groups.
    for i_group = 1:length(PMA_groups)
        pma_group = PMA_groups{i_group};
        
        % Loop over sleep stages.
        for i_sleep = 1:length(sleep_stages)
            ss = sleep_stages{i_sleep};
            
            % Get all filepaths of the group and sleep stage.
            filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
            
            % Loop over file paths.
            group_index = [];
            for i_file = 1:length(filepaths)
                % (Re)start EEGLAB without GUI.
                [ALLEEG EEG CURRENTSET] = eeglab('nogui');
                
                % Load the EEG.
                EEG = pop_loadset(filepaths{i_file});
                [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 0, 'gui','off');        % And make this a new set
                ALLEEG = eeg_store(ALLEEG, EEG, CURRENTSET);  % Store the thing
                i_current = CURRENTSET;
                
                % Load the mean templates.
                if strcmp(mean_group, 'outcome_sleepstage_PMAgroup')
                    filename_grp = sprintf('GroupMean_%s_%d_%s.set', outcome, i_group, ss);
                    filepath_grp = fullfile(grp_dir, filename_grp);
                else
                    error('Invalid mean_group "%s".', mean_group)
                end
                EEG = pop_loadset(filepath_grp);
                [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 0, 'gui','off');        % And make this a new set
                ALLEEG = eeg_store(ALLEEG, EEG, CURRENTSET);  % Store the thing
                group_mean_index = CURRENTSET;
                
                % Assign MS templates.
                % Set fit parameters.
                ALLEEG(i_current).msinfo.FitParams = FitPars;
                ClustPars = ALLEEG(i_current).msinfo.ClustPar;
                for k = ClustPars.MinClasses:ClustPars.MaxClasses
                    % - MSClass: N timepoints x N Segments matrix of momentary labels
                    % - gfp: N timepoints x N Segments matrix of momentary gFP values
                    % - fit: Relative variance explained by the model
                    [MSClass, gfp, fit] = AssignMStates(ALLEEG(i_current), ALLEEG(group_mean_index).msinfo.MSMaps(k).Maps, ALLEEG(i_current).msinfo.FitParams, true);
                    ALLEEG(i_current).msinfo.L_all{k,1} = MSClass';  % L_all:Lables: segments x time
                    ALLEEG(i_current).msinfo.gfp(1,:) = gfp;  % segments x time
                    ALLEEG(i_current).msinfo.fit(k,:) = fit;
                    ALLEEG(i_current).msinfo.A_all{k,1} = ALLEEG(group_mean_index).msinfo.MSMaps(k).Maps';  % A_all:MS Maps: channels x maps
                    
                    % Compution quality measure W (dispersion). W trends toward 0 as the quality of the clustering results increases
                    data = ALLEEG(i_current).data;
                    data = data(:,ALLEEG(i_current).msinfo.L_all{k,1}~=0);
                    tempL = ALLEEG(i_current).msinfo.L_all{k,1}(ALLEEG(i_current).msinfo.L_all{k,1}~=0);
                    w = CalcDispersion(data, ALLEEG(i_current).msinfo.A_all{k,1}, tempL);
                    W(k-(ClustPars.MinClasses-1)) = w; % Save dispersion for each k (number of kmeans clusters).
                    clear data;
                end
                % Krzanowski-Lai (KL) criterion: point where W has elbow point.
                NumK = length(W);  % Number of k's tried.
                C = ALLEEG(i_current).nbchan;
                for seg = 2:NumK-1 % We need to compute the curvature, so we'll work with derivatives, so cannot compute for first and last k.
                    DIFFk = (seg-1)^(2/C)*W(seg-1) - seg^(2/C)*W(seg);
                    DIFFk1 = seg^(2/C)*W(seg) - (seg+1)^(2/C)*W(seg+1);
                    KL(seg) = abs(DIFFk/DIFFk1);
                    ALLEEG(i_current).msinfo.KL{seg,1} = abs(DIFFk/DIFFk1);
                end
                
                % Save results.
                EEG = eeg_retrieve(ALLEEG, i_current);
                filename = EEG.filename;
                pop_saveset(EEG, 'filename', filename, 'filepath', grp_dir, 'savemode', 'onefile');
                
                count = count + 1;
                disp(count)
                
                clear ALLEEG EEG CURRENTSET
            end
        end
    end
end