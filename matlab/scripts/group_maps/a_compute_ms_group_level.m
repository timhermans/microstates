% This script loads the MS analysis of the individual level and computes
% group level microstates.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', 'microstates');

% Path to save the group level .set files.
output_dir = fullfile(set_dir, 'group_level');

%% Options.
outcomes = {'normal', 'moderate', 'severe'};
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

%% Process.
% Create output dir if it does not exist.
if ~isfolder(output_dir)
    mkdir(output_dir)
end

% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Add EEGLAB to path.
addpath(paths.eeglab_dir)

% Loop over outcomes.
all_group_mean_index = [];
for i_outcome = 1:numel(outcomes)
    outcome = outcomes{i_outcome};
    
    % Loop over PMA groups.
    for i_group = 1:length(PMA_groups)
        pma_group = PMA_groups{i_group};
        
        % Loop over sleep stages.
        for i_sleep = 1:length(sleep_stages)
            ss = sleep_stages{i_sleep};
            
            % Get all filepaths of the group and sleep stage.
            filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
            
            % (Re)start EEGLAB without GUI.
            [ALLEEG EEG CURRENTSET] = eeglab('nogui');
            
            % Load all EEG sets of the group.
            group_index = [];
            for i_file = 1:length(filepaths)
                EEG = pop_loadset(filepaths{i_file});
                [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 0, 'gui','off');        % And make this a new set
                ALLEEG = eeg_store(ALLEEG, EEG, CURRENTSET);  % Store the thing
                group_index = [group_index, CURRENTSET]; % And keep track of the groups
            end
            
            % Compute the group mean MS maps.
            EEG = findGroupMSTemplates(ALLEEG, group_index, strjoin({'GroupMean', outcome, pma_group, ss}, ' '));
            
            % Make new set.
            [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, numel(ALLEEG)+1, 'gui', 'off'); % Make a new set
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET); % and store it
            group_mean_index = CURRENTSET;
            all_group_mean_index = [all_group_mean_index group_mean_index]; % And keep track of it
            
            % Save results.
            EEG = eeg_retrieve(ALLEEG, group_mean_index);
            EEG.group = pma_group;
            filename = sprintf('GroupMean_%s_%d_%s', outcome, i_group, ss);
            pop_saveset(EEG, 'filename', filename, 'filepath', output_dir, 'savemode', 'onefile');
            
            clear ALLEEG EEG CURRENTSET
        end
    end
end

