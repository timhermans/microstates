% This scripts computes MS metrics for each recording using individual MS
% maps. Saves results to .csv files.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Path to folder with .set files with individual MS.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', ...
    'microstates');

% Path to output dir.
output_dir = set_dir;

%% Options.
outcomes = {'normal'};
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};
num_maps = 4;

% Defining fitting parameters
FitPars.b = 30;                   % the window size, if labels are smoothed
FitPars.lambda = 0.3;             % the non-smoothness penalty factor
FitPars.nClasses = num_maps;      % the number of classes to fit
FitPars.PeakFit = false;          % whether to fit only the GFP peaks and interpolate in between (true), or fit to the entire data (false)
FitPars.BControl = true;          % kill microstates truncated by boundaries.

%% Process.
% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Add EEGLAB to path.
addpath(paths.eeglab_dir)

% Loop over outcomes.
all_group_mean_index = [];
for i_outcome = 1:numel(outcomes)
    outcome = outcomes{i_outcome};
    
    % Loop over PMA groups.
    for i_group = 1:length(PMA_groups)
        pma_group = PMA_groups{i_group};
        
        % Loop over sleep stages.
        for i_sleep = 1:length(sleep_stages)
            ss = sleep_stages{i_sleep};
            
            % Get all filepaths of the group and sleep stage.
            filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
            
            % (Re)start EEGLAB without GUI.
            [ALLEEG EEG CURRENTSET] = eeglab('nogui');
            
            % Load all EEG sets of the group.
            group_index = [];
            for i_file = 1:length(filepaths)
                fp = filepaths{i_file};
                [~, filename] = fileparts(fp);
                rec_name = strip(replace(lower(filename(1: strfind(filename, '_preprocessed')-1)), 'pt', ''));
                EEG = pop_loadset(fp);
                EEG.subject = rec_name;
                EEG.group = pma_group;
                EEG.condition = ss;
                [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 0, 'gui','off');        % And make this a new set
                ALLEEG = eeg_store(ALLEEG, EEG, CURRENTSET);  % Store the thing
                group_index = [group_index, CURRENTSET]; % And keep track of the groups
            end

            % Assign and compute metrics (pop_QuantMSTemplates).
            if FitPars.PeakFit
                filename_out = sprintf('Metrics_GFP_%s_%d_%s.csv', ...
                    outcome, i_group, ss);
            else
                filename_out = sprintf('Metrics_%s_%d_%s.csv', ...
                    outcome, i_group, ss);               
            end
            pop_QuantMSTemplates(ALLEEG, group_index, 0, FitPars, ...
                [], fullfile(output_dir, filename_out));
            clear ALLEEG EEG CURRENTSET
        end
    end
end



