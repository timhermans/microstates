% This scripts extracts the GEV for each recording and 
% computes the Hurst exponent of the MS sequence. 
% Results are saved to a CSV file.

%% Initialization
clear;
close all;
clc;

% Set paths
this_dir = fileparts(mfilename('fullpath'));
root_dir = this_dir(1: 1 + end - strfind(reverse(lower(this_dir)), reverse('microstates')));
addpath(genpath(fullfile(root_dir, 'matlab', 'functions')))
paths = getPaths();

% Whether to use group_lavel maps (true) or individual maps (false).
group_level = false;

% Path to .set files. Either select the folder with individual microstates
% assigned or the group level microstates assigned.
set_dir = fullfile(paths.output_root_dir, 'preprocessed', 'epochs', ...
    'microstates');

if group_level
    % Change to group lavel directory.
    set_dir = fullfile(set_dir, 'group_level');
end

%% Options.
outcome = 'normal';
sleep_stages = {'QS', 'NQS'};
PMA_groups = {'<=31', '32-33', '34-36', '>=37'};

% Read recording data.
T_pd = readtable(paths.recording_data_file);

% Output filepath (.csv).
dir_out = fullfile('D:\data\processed\microstates\features\channel_average');
if group_level
    fname =  'GEV_hurst_group_level.csv';
else
    fname =  'GEV_hurst.csv';
end
fp_out = fullfile(dir_out, fname);
%% Process.
% All data will be collected in this array (n_recordings, n_features).
data = [];
rec_names_all = {};
sleep_stages_all = {};

% Loop over PMA groups.
for i_group = 1:length(PMA_groups)
    pma_group = PMA_groups{i_group};
    
    % Loop over sleep stages.
    for i_sleep = 1:length(sleep_stages)
        ss = sleep_stages{i_sleep};
        
        % Get all filepaths of the group and sleep stage.
        filepaths = getAllFilepaths(set_dir, outcome, pma_group, ss, T_pd);
        
        for i_file = 1:numel(filepaths)
            filepath_set = filepaths{i_file};
            [~, filename, ~] = fileparts(filepath_set);
            splits = split(filename, '_preprocessed_');
            rec_name = splits{1};
            sleep_stage = splits{2};

            % Only load the MS info (much faster than loading the entire file).
            load('-mat', filepath_set, 'msinfo')

            % Extract GEV (in percentage).
            GEV_i = msinfo.fit * 100;
            
            % Compute Hurst.
            H4 = computeHurst(msinfo.L_all{4});
            
            % Collect data.
            k = msinfo.ClustPar.MinClasses: msinfo.ClustPar.MaxClasses;
            data = [data; [reshape(GEV_i(k, :), 1, []), H4]];
            rec_names_all = [rec_names_all; rec_name];
            sleep_stages_all = [sleep_stages_all; sleep_stage];
        end
    end
end

% Create feature labels.
feature_labels = {};
for ii = 1:length(k)
    feature_labels{ii} = sprintf('GEV_%d', k(ii));
end
feature_labels{end+1} = 'Hurst_4';
assert(length(feature_labels) == size(data, 2))

T = array2table(data, "VariableNames", feature_labels);
T.sleep_stage = sleep_stages_all;
T = [cell2table(rec_names_all, 'VariableNames', {'rec_name'}), T];
disp(T)

writetable(T, fp_out, 'Delimiter', ',')

