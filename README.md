# Microstate analysis project.

## Dependencies: 
- EEGLAB
- Microstate analysis plugin for EEGLAB (https://www.thomaskoenig.ch/index.php/software/10-eeglab-plugin-manual)

## Structure 
For a detailed guide on how to use the code, see Practical guide.docx.
The main analysis is in MATLAB, with all main scripts in the 'scripts' folder. 
'functions' contains .m files with functions used by the main scripts. 
In the python folder, you can find some python functions and scripts which were made for additional visualizations.