"""
This module contains constants and defaults related to the paths of the project.
"""
import warnings
from os.path import abspath, dirname


__all__ = [
    'DATA_DIR',
    'BIOMEDDATA_DIR',
    'PROCESSED_DIR',
    'METADATA_DIR',
    'ROOT_DIR',
    'OUTPUT_DIR',
    'D1_DATA_DIR',
    'D2_DATA_DIR',
]

this_dirpath = dirname(abspath(__file__))


def on_pc_thermans():
    return r'c:\users\thermans' in this_dirpath.lower()


def on_server_thermans():
    return '/users/sista/thermans/' in this_dirpath.lower()


def up(path, n=1):
    # Go n times up a directory in path.
    for _ in range(n):
        path = dirname(path)
    return path


# Device-common paths.
root_dir = up(this_dirpath, 1)

# Device-specific paths.
if on_pc_thermans():
    # Path to the directory with (EDF) data.
    data_dir = r'D:\data\raw'

    # Path to the biomeddata directory on the server.
    biomeddata_dir = 'Y:'

    sista_dir = 'X:'

    # Path to data/processed directory.
    processed_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\processed'

    # Path to data/metadata directory.
    metadata_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\metadata'

    # Path to output root.
    output_dir = r'D:\data\processed\microstates\preprocessed'

    # Paths to EDF files.
    D1_data_dir = r'W:\sensitive\Leuven_pretermEEG\Leuven_pretermEEG'
    D2_data_dir = r'D:\data\raw\Neonatal-preterm-maturation\neonatal-preterm-maturation-43'

elif on_server_thermans():
    # Path to the directory with (EDF) data on the server.
    data_dir = '/esat/stadiusdata/sensitive'

    # Path to the biomeddata directory.
    biomeddata_dir = '/esat/biomeddata'

    # Path to sista user directory.
    sista_dir = '/users/sista/thermans'

    # Path to data/processed directory.
    processed_dir = '/esat/biomeddata/thermans/processed'

    # Path to data/metadata directory.
    metadata_dir = '/users/sista/thermans/data/metadata'

    # Path to output root.
    output_dir = '/esat/biomeddata/thermans/output'

    # Paths to EDF files.
    D1_data_dir = '/esat/stadiusdata/sensitive/Leuven_pretermEEG/Leuven_pretermEEG'
    D2_data_dir = '/esat/stadiusdata/sensitive/Neonatal-preterm-maturation/neonatal-preterm-maturation-43'

else:
    # Do not set the data paths if platform cannot be deduced.
    msg = ('Unknown platform so cannot set the data paths (this_dirpath="{}")'
           .format(this_dirpath))
    warnings.warn(msg)
    data_dir = ''
    biomeddata_dir = ''
    sista_dir = ''
    processed_dir = ''
    metadata_dir = ''
    output_dir = ''
    D1_data_dir = ''
    D2_data_dir = ''

# Convert relative paths to absolute paths.
DATA_DIR = abspath(data_dir)
BIOMEDDATA_DIR = abspath(biomeddata_dir)
SISTA_DIR = abspath(sista_dir)
PROCESSED_DIR = abspath(processed_dir)
METADATA_DIR = abspath(metadata_dir)
ROOT_DIR = abspath(root_dir)
OUTPUT_DIR = abspath(output_dir)
D1_DATA_DIR = abspath(D1_data_dir)
D2_DATA_DIR = abspath(D2_data_dir)
