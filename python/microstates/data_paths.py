import os

import pandas as pd

from microstates.path_config import METADATA_DIR, D1_DATA_DIR, D2_DATA_DIR


def get_recording_data(add_filepaths=True, **kwargs):
    """
    Shortcut to load the patient data and return it as a pandas DataFrame.

    Args:
        add_filepaths (bool): if True, adds the filepaths to the EDF files to the DataFrame in an extra column.

    Returns:
        df (pd.DataFrame): DataFrame with patient/recording information.
    """
    # Filepath to the patient data Excel.
    filepath = os.path.join(METADATA_DIR, 'microstates', 'recording_data.xlsx')

    # Read Excel.
    df = pd.read_excel(filepath, **kwargs)

    # Add filepaths to EDFs if requested.
    if add_filepaths:
        df['filepath'] = df.apply(lambda row: get_edf_filepath(row['dataset'], row['filename']), axis=1)

    return df


def get_edf_filepath(dataset, filename):
    """
    Return a list of EDF filepaths.

    Args:
        dataset (str): 'D1' or 'D2'.
        filename (str): filename of the EDF file (incl. extension).

    Returns:
        filepath (str): filepath to the EDF file.
    """
    if dataset == 'D1':
        data_dir = D1_DATA_DIR
    elif dataset == 'D2':
        data_dir = D2_DATA_DIR
    else:
        raise ValueError(f"Unknown dataset '{dataset}'")
    return os.path.join(data_dir, filename)

