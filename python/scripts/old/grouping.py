"""
This script ...

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from microstates.data_paths import get_recording_data
from microstates.path_config import METADATA_DIR

plt.close('all')
plt.style.use('pres')

# Read excel.
df = get_recording_data(add_filepaths=False)

# Create variable dataset_outcome.
df['dataset_outcome'] = df['dataset'] + ' ' + df['outcome']
groups_all = df['dataset_outcome'].unique()
groups_all = list(groups_all[[0, 1, 3, 2]])

# Define PMA range.
pma_max = int(np.floor(df.PMA.max()))
pma_min = int(np.floor(df.PMA.min()))
pma_all = np.arange(pma_min, pma_max, 1)

# Initialize count dataframes.
counts_pma = pd.DataFrame(data=np.zeros((len(groups_all), len(pma_all)), int), index=groups_all, columns=pma_all)
counts_pma_first = pd.DataFrame(data=np.zeros((len(groups_all), len(pma_all)), int), index=groups_all, columns=pma_all)
grouping_1 = pd.DataFrame(data=np.zeros((len(groups_all), 4), int), index=groups_all, columns=['<=32', '33-34', '35-36', '>=37'])
grouping_2 = pd.DataFrame(data=np.zeros((len(groups_all), 3), int), index=groups_all, columns=['<=32', '33-36', '>=37'])
grouping_3 = pd.DataFrame(data=np.zeros((len(groups_all), 3), int), index=groups_all, columns=['<=31', '32-36', '>=37'])
grouping_4 = pd.DataFrame(data=np.zeros((len(groups_all), 4), int), index=groups_all, columns=['<=31', '32-33', '34-36', '>=37'])
pat_nums_1 = [[], [], [], []]
pat_nums_2 = [[], [], []]
pat_nums_3 = [[], [], []]
pat_nums_4 = [[], [], [], []]

# Loop over rows and find out to which cell it belongs in the count matrix.
for _, row in df.iterrows():
    pma = row['PMA']
    pat_num = row['pat_num']
    ii = groups_all.index(row['dataset_outcome'])
    jj = np.argmax((pma_all - pma) > 0) - 1
    # jj = np.argmin(np.abs(pma_all - row['PMA']))  # Closest PMA.
    counts_pma.iat[ii, jj] += 1

    # print(row['PMA'], pma_all[jj])

    # Check if it is the first recording for the current patient.
    pmas_pat = df[df['pat_num'] == row['pat_num']].PMA
    if np.all(pmas_pat >= pma):
        counts_pma_first.iat[ii, jj] += 1

    # Group 1.
    if pma <= 32.99:
        jj_1 = 0
    elif pma <= 34.99:
        jj_1 = 1
    elif pma <= 36.99:
        jj_1 = 2
    else:
        jj_1 = 3
    if pat_num not in pat_nums_1[jj_1]:
        grouping_1.iat[ii, jj_1] += 1
        pat_nums_1[jj_1].append(pat_num)

    # Group 2.
    if pma <= 32.99:
        jj_2 = 0
    elif pma <= 36.99:
        jj_2 = 1
    else:
        jj_2 = 2
    if pat_num not in pat_nums_2[jj_2]:
        grouping_2.iat[ii, jj_2] += 1
        pat_nums_2[jj_2].append(pat_num)

    # Group 3.
    if pma <= 31.99:
        jj_3 = 0
    elif pma <= 36.99:
        jj_3 = 1
    else:
        jj_3 = 2
    if pat_num not in pat_nums_3[jj_3]:
        grouping_3.iat[ii, jj_3] += 1
        pat_nums_3[jj_3].append(pat_num)

    # Group 4.
    if pma <= 31.99:
        jj_4 = 0
    elif pma <= 33.99:
        jj_4 = 1
    elif pma <= 36.99:
        jj_4 = 2
    else:
        jj_4 = 3
    if pat_num not in pat_nums_4[jj_4]:
        grouping_4.iat[ii, jj_4] += 1
        pat_nums_4[jj_4].append(pat_num)

# Remove all zero columns in first PMA.
counts_pma_first = counts_pma_first.iloc[:, :counts_pma_first.shape[-1] - np.argmax(counts_pma_first.sum()[::-1] > 0)]

print('\nNumber of PMA:')
print(counts_pma)
fig, axes = plt.subplots(nrows=len(groups_all), ncols=1, tight_layout=True, sharex='all')
for ax, group in zip(axes, groups_all):
    ax.bar(x=counts_pma.columns.to_list(), height=counts_pma.loc[group, :].values)
    ax.set_title(group)
    ax.set_ylabel('Count')
    ax.grid()
ax.set_xlabel('PMA (weeks)')

print('\nNumber of first PMA:')
print(counts_pma_first)
fig, axes = plt.subplots(nrows=len(groups_all), ncols=1, tight_layout=True, sharex='all')
for ax, group in zip(axes, groups_all):
    ax.bar(x=counts_pma_first.columns.to_list(), height=counts_pma_first.loc[group, :].values)
    ax.set_title(group)
    ax.set_ylabel('Count')
    ax.grid()
ax.set_xlabel('PMA (weeks)')

print('\nGrouping 1:')
print(grouping_1)

print('\nGrouping 2:')
print(grouping_2)

print('\nGrouping 3:')
print(grouping_3)

print('\nGrouping 4:')
print(grouping_4)

# with pd.ExcelWriter('PMA counts.xlsx') as writer:
#     counts_pma.to_excel(writer, sheet_name='All')
#     counts_pma_first.to_excel(writer, sheet_name='First')
#     grouping_1.to_excel(writer, sheet_name='Grouping 1')
#     grouping_2.to_excel(writer, sheet_name='Grouping 2')
#     grouping_3.to_excel(writer, sheet_name='Grouping 3')
#     grouping_4.to_excel(writer, sheet_name='Grouping 4')
