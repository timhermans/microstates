"""
This script computes sleep stages using a CNN model.
Additionally, it preprocessed each EEG (filtering, resampling, referencing) for later visualization.
The script saves the sleep stages and the preprocessed data to HDF5 files.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os
import sys
import warnings

import numpy as np

import pyprind

from microstates.data_paths import get_recording_data
from microstates.path_config import OUTPUT_DIR
from nnsa import EdfReader, Butterworth


def main(i_batch, num_batches=20):
    # Path to output directory.
    output_dir = os.path.join(OUTPUT_DIR)

    # List of all recordings incl. the filepaths of the recordings.
    df = get_recording_data(add_filepaths=True)

    # Extract the current batch.
    nrows = df.shape[0]
    batch_size = int(np.ceil(nrows/num_batches))
    start = i_batch * batch_size
    stop = min([start + batch_size, nrows])
    df = df.iloc[start:stop, :]

    # List of EEG channels to read from EDF.
    channel_list = ['Fp1', 'Fp2', 'C3', 'C4', 'Cz', 'T3', 'T4', 'O1', 'O2']

    # Define filter.
    filt = Butterworth(order=7, passband=[0.2, 25])

    # Define resampling frequency (Hz).
    fs_resample = 100

    # Loop over recordings, compute sleep stages and save.
    bar = pyprind.ProgBar(len(df), stream=sys.stdout)
    for _, row in df.iterrows():
        # Check if EDF file exist.
        fp = row['filepath']
        if not os.path.exists(fp):
            msg = f'\nFile "{fp}" does not exist. SKipping...'
            warnings.warn(msg)
            continue

        # Create output filepaths.
        rec_name = os.path.splitext(row['filename'])[0]
        fp_out_ss = os.path.join(output_dir, 'sleep_stages', f'{rec_name}_sleep_stages_cnn_2class.hdf5')
        fp_out_pp = os.path.join(output_dir, 'preprocessed', f'{rec_name}_preprocessed.hdf5')

        # Check if outputs already exist.
        if all([os.path.exists(f) for f in [fp_out_ss, fp_out_pp]]):
            print(f'File "{fp}" already processed. Skipping...' )
            continue

        # Read EEG data.
        print(f'Reading {fp}...')
        with EdfReader(fp) as r:
            eeg_ds = r.read_eeg_dataset(discontinuous_mode='longest', dtype=np.float32)

        # Only keep relevant channels.
        eeg_ds = eeg_ds.extract_channels(channels=channel_list, make_copy=False)

        # Compute sleep stages (2 class).
        if not os.path.exists(fp_out_ss):
            sleep_stages = eeg_ds.sleep_stages_cnn(preprocess=True, verbose=1, num_classes=2)

            # Save sleep stages.
            sleep_stages.save_to_file(fp_out_ss)

        # Filter, resample and reference.
        if not os.path.exists(fp_out_pp):
            eeg_ds.filtfilt(filt, inplace=True)
            eeg_ds.resample(fs_new=fs_resample, inplace=True)
            eeg_ds.reference('mean', inplace=True)
            eeg_ds.save_hdf5(fp_out_pp, overwrite=True)

        # Update progress bar.
        bar.update()


if __name__ == "__main__":
    args = sys.argv[1:]  # First argument is the script filename.
    if len(args) > 0:
        all_i_batch = []
        for i in range(len(args)):
            i_batch = int(args[i])
            all_i_batch.append(i_batch)
    else:
        all_i_batch = list(range(20))

    list(map(main, all_i_batch))
