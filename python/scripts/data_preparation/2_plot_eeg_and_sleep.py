"""
This script plots the preprocessed EEG and the sleep stages for visual identification of clean segments.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import matplotlib.pyplot as plt

import pandas as pd
from matplotlib.widgets import Button

from microstates.path_config import METADATA_DIR
from nnsa import EegDataset, read_result_from_file
from nnsa.utils import maximize_figure
from nnsa.utils.paths import select_path

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

# Directory with preprocessed and sleep stages HDF5 files.
preprocessed_dir = r'D:\data\proprocessed\microstates\preprocessed'
sleep_stages_dir = r'D:\data\proprocessed\microstates\sleep_stages'
metadata_dir = os.path.join(METADATA_DIR, 'microstates')

# Determine recording name from Excel: find first emtpy QS start.
df = pd.read_excel(os.path.join(metadata_dir, 'epoch_selection.xls'), engine="xlrd")
rec_name = os.path.splitext(df['filename'][df['start_QS'].isna()].values[0])[0]

# Print progress.
print('{:.2f} %'.format(df['start_QS'].notnull().mean()*100))

# Create EEG filename.
if rec_name is not None:
    filepath_eeg = os.path.join(preprocessed_dir, f'{rec_name}_preprocessed.hdf5')

# Open dialog and ask to select a file if rec_name is None or not found.
if rec_name is None or not os.path.exists(filepath_eeg):
    filepath_eeg = select_path('select_file', initialdir=preprocessed_dir)
    rec_name = os.path.splitext(os.path.basename(filepath_eeg))[0].replace('_preprocessed', '')

# Create sleep stages filename.
filepath_ss = os.path.join(sleep_stages_dir, f'{rec_name}_sleep_stages_cnn_2class.hdf5')

# Load EEG and sleep stages.
print('Opening {}...'.format(rec_name))
eeg_ds = EegDataset.read_hdf5(filepath_eeg)
sleep_stages_cnn = read_result_from_file(filepath_ss)

# Plot.
ax = eeg_ds.plot(scale=250, end=15*3600)
plt.gcf().canvas.set_window_title(rec_name)
ylim = ax.get_ylim()
sleep_stages_cnn.to_annotation_set().shade_axis()
plt.subplots_adjust(bottom=0.2)
maximize_figure()


# Menu with buttons.
class Menu(object):
    def __init__(self, ax=None):
        if ax is None:
            ax = plt.gca()
        self.ax = ax

    def snap_window(self, event):
        ax = self.ax
        xlim = ax.get_xlim()
        ax.set_xlim(xlim[0], xlim[0] + 350)
        ax.set_ylim(ylim)
        plt.draw()


# Add a 1 hour line to indicate time scale.
t_start = 0
t_stop = t_start + 3600
yline = plt.ylim()[1]
plt.plot([t_start, t_stop], [yline, yline], linewidth=10, color='r')

# Add menu buttons to plot.
menu = Menu(ax=ax)
bwin = Button(plt.axes([0.81, 0.05, 0.1, 0.075]), '5 min')
bwin.on_clicked(menu.snap_window)


