"""
This script ...

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os
import sys

import numpy as np

import pandas as pd
import pyprind

from microstates.path_config import METADATA_DIR

from nnsa import EegDataset

# Directory with preprocessed and sleep stages HDF5 files.
preprocessed_dir = r'D:\data\cache\microstates\preprocessed'
sleep_stages_dir = r'D:\data\cache\microstates\sleep_stages'
metadata_dir = os.path.join(METADATA_DIR, 'microstates')

# Load dataframe with epoch times. Saving as .xsl makes it possible to have the Excel file
# opened in Excel and still be able to read in this script.
df = pd.read_excel(os.path.join(metadata_dir, 'epoch_selection.xls'), engine="xlrd")

bar = pyprind.ProgBar(len(df), stream=sys.stdout)

# Loop over recordings and save to mat.
for idx, row in df.iterrows():
    rec_name = os.path.splitext(row['filename'])[0]

    filepath_qs = os.path.join(preprocessed_dir, 'epochs', f'{rec_name}_preprocessed_QS.mat')
    filepath_nqs = os.path.join(preprocessed_dir, 'epochs', f'{rec_name}_preprocessed_NQS.mat')

    if os.path.exists(filepath_qs) and os.path.exists(filepath_nqs):
        bar.update()
        continue

    # Create EEG filename.
    filepath_eeg = os.path.join(preprocessed_dir, f'{rec_name}_preprocessed.hdf5')

    # Load EEG and sleep stages.
    print('Opening {}...'.format(rec_name))
    ds = EegDataset.read_hdf5(filepath_eeg)

    # Extract and save QS and NQS epochs.
    if not np.isnan(row['start_QS']) and row['start_QS'] > 0:
        ds_qs = ds.extract_epoch(begin=row['start_QS'], end=row['stop_QS'])
        ds_qs.save_mat(filepath_qs, overwrite=True)
    if not np.isnan(row['start_NQS']) and row['start_NQS'] > 0:
        ds_nqs = ds.extract_epoch(begin=row['start_NQS'], end=row['stop_NQS'])
        ds_nqs.save_mat(filepath_nqs, overwrite=True)

    bar.update()
