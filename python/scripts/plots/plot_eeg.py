"""
This script plots an EEG epoch.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from nnsa import EegDataset
from nnsa.utils.plotting import scale_figsize, save_fig_as

plt.close('all')
try:
    plt.style.use('ms_paper')
except OSError:
    pass

import matplotlib.font_manager as font_manager
font_dir = [r'C:\Users\thermans\.matplotlib\fonts']
for font in font_manager.findSystemFonts(font_dir):
    font_manager.fontManager.addfont(font)

# For saving.
fig_width = 16
fig_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures'
save_fig = False

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Settings.
# Directory with preprocessed .mat files.
preprocessed_dir = r'D:\data\processed\microstates\preprocessed'
epochs_dir = os.path.join(preprocessed_dir, 'epochs')

rec_name = 'PT 112_1'

#%% Load and plot QS and NQS.
fig, axes_ = plt.subplots(2, 1, tight_layout=True, sharey='all', squeeze=False,
                          figsize=scale_figsize((19.2, 12), width=fig_width, unit='cm'),
                          )
axes = np.reshape([axes_], -1)
plot_kwargs = dict(scale=125, relative_time=True, linewidth=0.5)
for ax, sleep_stage in zip(axes, ['QS', 'NQS']):
    fp = os.path.join(epochs_dir, f'{rec_name}_preprocessed_{sleep_stage}.mat')

    # Load EEG.
    eeg_ds = EegDataset.read_mat(fp)
    eeg_ds.plot(ax=ax, **plot_kwargs)
    ax.set_title(sleep_stage)

    if ax not in axes_[-1, :]:
        ax.set_xlabel('')

if save_fig:
    save_fig_as(figname='example_epoch'+rec_name, directory=fig_dir, info=__file__)
