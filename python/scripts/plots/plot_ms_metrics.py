"""
This script plots MS metrics for different age groups.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from microstates.data_paths import get_recording_data
from microstates.path_config import OUTPUT_DIR
from nnsa.utils import subplot_rows_columns
from nnsa.utils.plotting import stripboxplot
from scipy.stats import f_oneway

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

#%% Set paths.
# Path to directory with the metrics (csv) files.
metrics_dir = os.path.join(OUTPUT_DIR, 'epochs', 'microstates', 'group_level', 'sorted')

#%% Settings.
# Whether to use the metrics computed using GFP (True) or continuous (False) backfit.
gfp_backfit = False

# Data selection.
outcome = 'normal'
sleep_stages_all = ['QS', 'NQS']
pma_groups_all = ['<=31', '32-33', '34-36', '>=37']

#%% Load metrics.
filepath_metrics = os.path.join(r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\RStudio', 'results_long_format.csv')
df = pd.read_csv(filepath_metrics)

df_rd = get_recording_data()
indices = [q.lower().replace('pt', '').replace('.edf', '').strip() for q in df_rd['filename']]
df_rd.index = indices
pmas = df_rd['PMA']
df['PMA'] = df.apply(lambda row: pmas[row['Recording name']], axis=1)


#%% Functions.
def plot_metrics(df, metrics_all, condition, xlabel='Age group', ylabel=None,
                 **kwargs):
    # Select condition.
    df = df[df['Condition'] == condition]

    # Default fig kwargs.
    fig_kwargs = dict(sharex='all', tight_layout=True, **kwargs)

    # Init plot.
    nrows, ncols = subplot_rows_columns(len(metrics_all))
    fig, axes = plt.subplots(nrows, ncols, **fig_kwargs)
    axes = np.reshape([axes], -1)

    # Loop over maps and plot.
    for ii, (metric_i, ax) in enumerate(zip(metrics_all, axes)):
        stripboxplot(x='Group', y=metric_i, data=df, ax=ax)
        if ii < len(axes) - ncols:
            ax.set_xlabel('')
        elif xlabel is not None:
            ax.set_xlabel(xlabel)
        if ii % ncols != 0 and fig_kwargs.get('sharey', 'none') == 'all':
            ax.set_ylabel('')
        else:
            if ylabel is None:
                ylab = metric_i
            elif isinstance(ylabel, str):
                ylab = ylabel
            else:
                ylab = ylabel[ii]
            ax.set_ylabel(ylab)

        # Compute one-way ANOVA pvalue.
        groups = []
        unique_groups = df['Group'].unique()
        for gr in unique_groups:
            df_i = df[df['Group'] == gr]
            groups.append(df_i[metric_i].values)
        stat, pval = f_oneway(*groups)

        ax.set_title('{}\np={:.3f}{}'.format(metric_i.replace('_', ' '),
                                           pval, '*' if pval < 0.05 else ''))
    return axes


def plot_per_ms(df, metric, condition, n_maps=4, xlabel='Age group', ylabel=None, **kwargs):
    metrics_all = [f'{metric}_{i_map + 1}' for i_map in range(n_maps)]
    return plot_metrics(df, metrics_all, condition, xlabel=xlabel, ylabel=ylabel,
                        sharey='all', figsize=[8, 8], **kwargs)


def plot_transitions(df, metric, condition, n_maps=4, xlabel='Age group', ylabel=None, **kwargs):
    metrics_all = []
    for ii in range(n_maps):
        for jj in range(n_maps):
            name = f'{metric}TM_{ii+1}->{jj+1}'
            metrics_all.append(name)
    return plot_metrics(df, metrics_all, condition, xlabel=xlabel, ylabel=ylabel,
                        sharey='all', **kwargs)

#%%
plt.close('all')
sns.histplot(x='DataSet', data=df.groupby('ID').count()/2, bins=np.arange(0.5, 5))
plt.title('Recording count')
plt.xlabel('Number of recordings')
plt.ylabel('Number of subjects')

# Plot.
for ss in sleep_stages_all:
    plot_per_ms(df, 'Duration', ss, ylabel='Duration (s)')
    plt.suptitle(ss)
    plot_per_ms(df, 'Occurrence', ss, ylabel='Occurrence (Hz)')
    plt.suptitle(ss)
    plot_per_ms(df, 'Coverage', ss, ylabel='Proportion (-)')
    plt.suptitle(ss)
    plot_metrics(df, ['MeanDuration', 'MeanOccurrence'], ss,
                 ylabel=['Duration (s)', 'Occurrence (Hz)'])
    plt.suptitle(ss)

    plot_transitions(df, 'Org', ss, ylabel='Proportion (-)', figsize=[13, 8])
    plt.suptitle(ss)
    plot_transitions(df, 'Exp', ss, ylabel='Proportion (-)', figsize=[13, 8])
    plt.suptitle(ss)
    plot_transitions(df, 'Delta', ss, ylabel='Proportion (-)', figsize=[13, 8])
    plt.suptitle(ss)

