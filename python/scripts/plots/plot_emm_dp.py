"""
This script plots estimated marginal means for directional predominance.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

#%% Settings.
# Directory containing the Excel files with the estimated marginal means (as computed by SPSS).
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'

#%% Process.
age_groups = ['<=31', '32-33', '34-36', '>=37']

# x-spacing between bars.
dx = 1.2

# Init fig.
fig, axes = plt.subplots(1, 2, tight_layout=True, figsize=1.4*np.array((7.5, 3.5)),
                         squeeze=False, sharey='row')

# Loop over metrics.
for i_metric, metric in enumerate(['dp']):
    # Load results.
    fp = os.path.join(output_dir, f'emm_{metric.lower()}.xlsx')
    df = pd.read_excel(fp).fillna(method='ffill')
    df['Microstates'] = df['Microstates'].str.replace('<->', '')

    for i_ss, ss in enumerate(['QS', 'NQS']):
        # Select axes.
        ax = axes[i_metric, i_ss]

        # Select data.
        df_ss = df[df['SleepStage'] == ss]

        # Initiate x-coord.
        xc = 0

        # Loop over microstates.
        xticks = []
        xlabels = []
        legend_elements = []
        for ms in df_ss['Microstates'].unique():
            df_ms = df_ss[df_ss['Microstates'] == ms]

            # Loop over age groups.
            x_coords = []
            for i_gr, gr in enumerate(age_groups):
                df_gr = df_ms[df_ms['AgeGroup'] == gr]

                if metric == 'Coverage':
                    # To percentage.
                    df_gr[['Lower Bound', 'Upper Bound', 'Mean']] *= 100

                # Y-coords of patch.
                yc_i = [df_gr['Lower Bound'].values, df_gr['Upper Bound'].values]

                # Draw patch.
                color = f'C{i_gr}'
                rect_i = Rectangle((xc, yc_i[0]), dx*0.85, yc_i[1]-yc_i[0],
                                   color=color, alpha=1)
                ax.add_patch(rect_i)
                legend_elements.append(Line2D([0], [0], color=color,
                                              lw=4, label=gr))

                # Draw mean.
                ax.plot((xc + xc+dx*0.85)/2 + np.array([-dx*0.85/2, dx*0.85/2]), [df_gr['Mean'].values]*2, color='k')
                x_coords.extend([xc, xc+1])

                # Update x-coord.
                xc += dx

            # Update X-coord (creates gap between MS).
            xc += 2.5*dx
            xticks.append(np.mean(x_coords))
            xlabels.append(ms)

        # Set xticks and labels.
        plt.sca(ax)
        ax.set_title('{}'.format(ss))
        ax.set_xlabel('Microstates')
        plt.xticks(ticks=xticks, labels=xlabels)

        if metric == 'dp':
            ax.set_ylabel('Directional predominance (-)')

axes[0, -1].legend(handles=legend_elements[:len(age_groups)],
                   loc='center left', bbox_to_anchor=(0.8, 1))
for ax in axes[:-1, :].reshape(-1):
    ax.set_xlabel('')