"""
This script creates a scatter plot for MS metric per sleep stage.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr

from nnsa.utils import check_directory_exists

plt.close('all')
try:
    plt.style.use('ms_paper')
except OSError:
    pass

import matplotlib.font_manager as font_manager
font_dir = [r'C:\Users\thermans\.matplotlib\fonts']
for font in font_manager.findSystemFonts(font_dir):
    font_manager.fontManager.addfont(font)

#%% Settings.
# Directory containing the Excel files with the estimated marginal means (as computed by SPSS).
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'
save_fig = True

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Settings.
# Whether to use the results found with individual (False) or group level (True) MS maps.
use_group_results = False

# Filepath to the results per microstate Excel.
if use_group_results:
    filepath_mean = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\results_mean.csv'
else:
    filepath_mean = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\individual_results_mean.csv'

#%% Mean duration across all microstates.
# Load metrics.
df = pd.read_csv(filepath_mean)

# Determine number of microstates.
n_maps = df.filter(like='Duration').shape[1] - 1

# Compute Chi-square distance of probabilities to measure structure
# (distance from random, i.e. the higher the chi-square, the further away the
# MS transitions are from random).
df['Chi-square'] = 0
for x in range(1, n_maps+1):
    for y in range(1, n_maps+1):
        if x == y:
            continue
        p_obs = df['OrgTM_{}->{}'.format(x, y)]
        p_exp = df['ExpTM_{}->{}'.format(x, y)]
        dist = (p_obs - p_exp)**2/p_exp
        df['Chi-square'] += dist

# Init fig.
metrics_all = ['Duration', 'Occurrence']  # , 'Chi-square'
fig, axes = plt.subplots(len(metrics_all), 2, tight_layout=True,
                         figsize=1*np.array((5.5, 5.5*len(metrics_all)/3 + 1)),
                         sharey='row', sharex='all')

# Loop over metrics.
for i_metric, metric in enumerate(metrics_all):
    if metric != 'Chi-square':
        y = 'Mean{}'.format(metric)
    else:
        y = metric

    for i_ss, ss in enumerate(['QS', 'NQS']):
        # Select axes.
        ax = axes[i_metric, i_ss]

        # Select data.
        df_ss = df[df['SleepStage'] == ss]

        # Plot.
        sns.regplot(x='PMA', y=y, data=df_ss, ax=ax)

        # Compute pearson correlation.
        r, p = pearsonr(df_ss['PMA'], df_ss[y])

        # Set xticks and labels.
        plt.sca(ax)
        ax.set_title('{} ({})\nr={:.2f}, p={:.2g}'.format(metric, ss, r, p))
        ax.set_xlabel('PMA (weeks)')

        if metric == 'Duration':
            ax.set_ylabel('Duration (s)')
        elif metric == 'Occurrence':
            ax.set_ylabel('Occurrence (Hz)')
        else:
            ax.set_ylabel(f'{metric} (-)')

for ax in axes[:-1, :].reshape(-1):
    ax.set_xlabel('')

#%% Process per MS (if using group results).
if use_group_results:
    filepath_per_ms = filepath_mean.replace('_mean', '_per_ms')

    # Load metrics.
    df = pd.read_csv(filepath_per_ms)

    # Loop over metrics.
    for i_metric, metric in enumerate(['Duration', 'Occurrence', 'Coverage']):
        # Init fig.
        fig, axes = plt.subplots(2, 2, tight_layout=True, sharex='all',
                                 figsize=1.4 * np.array((5.5, 4.5)), sharey='row')
        axes = np.reshape([axes], -1)

        # Loop over MS.
        for i_ms, ms in enumerate(df['Microstate'].unique()):
            df_ms = df[df['Microstate'] == ms]
            ax = axes[i_ms]

            for i_ss, ss in enumerate(['QS', 'NQS']):
                # Select data.
                df_ss = df_ms[df_ms['SleepStage'] == ss]

                # Plot.
                sns.regplot(x='PMA', y=metric, data=df_ss, ax=ax, label=ss)

            # Set xticks and labels.
            plt.sca(ax)
            ax.set_title('{}'.format(ms))
            ax.set_xlabel('PMA (weeks)')

            if metric == 'Duration':
                ax.set_ylabel('Duration (s)')
            elif metric == 'Occurrence':
                ax.set_ylabel('Occurrence (Hz)')
            elif metric == 'Coverage':
                ax.set_ylabel('Coverage (%)')
        axes[1].legend(loc='center left', bbox_to_anchor=(0.8, 1))
        for ax in axes:
            ax.set_xlabel('')

        plt.suptitle(metric)

if save_fig:
    fp_out = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures\fig_regression.eps'
    check_directory_exists(filepath=fp_out)
    plt.savefig(fp_out, format='eps')
    plt.savefig(fp_out.replace('.eps', '.tiff'), dpi=300)
