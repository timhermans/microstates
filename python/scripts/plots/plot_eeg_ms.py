"""
This script plots EEG with MS in the background.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import scipy
import seaborn as sns

from nnsa import EegDataset
from nnsa.utils.event_detections import get_onsets_offsets
from nnsa.utils.plotting import scale_figsize, save_fig_as

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass


# For saving.
fig_width = 16
fig_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Thesis\thesis\chapters\sigproc\image'
save_fig = False

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Settings.
# Directory with preprocessed .mat files.
preprocessed_dir = r'D:\data\processed\microstates\preprocessed'
epochs_dir = os.path.join(preprocessed_dir, 'epochs', 'microstates', 'group_level')

rec_name = 'PT 274_2'

#%% Load and plot.
sleep_stage = 'NQS'
fig, ax = plt.subplots(1, 1, tight_layout=True, sharey='all', squeeze=True,
                          figsize=scale_figsize((19.2, 12), width=fig_width, unit='cm'),
                          )
fp = os.path.join(epochs_dir, f'{rec_name}_preprocessed_{sleep_stage}.set')

# Read data.
loadmat_kwargs = dict({
    'struct_as_record': False,
    'squeeze_me': True,
    'mat_dtype': False,
})

# Load sequence of MS.
k = 4
data = scipy.io.loadmat(fp, **loadmat_kwargs)
ms_sequence = data['msinfo'].L_all[k-1]

# Load EEG.
eeg_ds = EegDataset.read_set(fp)

#%% Plot.
plot_kwargs = dict(scale=125, relative_time=True, linewidth=0.5, color='k')
t = np.arange(len(ms_sequence))/eeg_ds.fs
eeg_ds.plot(ax=ax, **plot_kwargs)
ax.set_title(sleep_stage)

onsets, offsets = get_onsets_offsets(ms_sequence[:-1] == ms_sequence[1:])
for idx_on, idx_off in zip(onsets, offsets):
    period = ms_sequence[idx_on: idx_off+1]
    ki = np.mean(period)
    if not ki.is_integer():
        raise AssertionError(ki)
    color = f'C{int(ki)}'
    ax.axvspan(t[idx_on], t[idx_off], color=color, zorder=-100)

if save_fig:
    save_fig_as(figname='ms_'+rec_name, directory=fig_dir, info=__file__)

