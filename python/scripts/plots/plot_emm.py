"""
This script plots estimated marginal means for duration, occurrence and coverage.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

from nnsa.utils import check_directory_exists
from nnsa.utils.plotting import scale_figsize, save_fig_as

plt.close('all')
try:
    plt.style.use('ms_paper')
except OSError:
    pass
#
# import matplotlib.font_manager as font_manager
# font_dir = [r'C:\Users\thermans\.matplotlib\fonts']
# for font in font_manager.findSystemFonts(font_dir):
#     font_manager.fontManager.addfont(font)

#%% Settings.
# Directory containing the Excel files with the estimated marginal means (as computed by SPSS).
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'

# For saving.
fig_width = 12
fig_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures'
save_fig = True

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Process.
age_groups = ['<=31', '32-33', '34-36', '>=37']

# x-spacing between bars.
dx = 1.2

# Init fig.
fig, axes = plt.subplots(3, 2, tight_layout=True,
                         figsize=scale_figsize((5.5, 6.5), width=fig_width, unit='cm'),
                         sharey='row')

# Loop over metrics.
for i_metric, metric in enumerate(['Duration', 'Occurrence', 'Coverage']):
    # Load results.
    fp = os.path.join(output_dir, f'emm_{metric.lower()}.xlsx')
    df = pd.read_excel(fp).fillna(method='ffill')

    for i_ss, ss in enumerate(['QS', 'NQS']):
        # Select axes.
        ax = axes[i_metric, i_ss]

        # Select data.
        df_ss = df[df['SleepStage'] == ss]

        # Initiate x-coord.
        xc = 0

        # Loop over microstates.
        xticks = []
        xlabels = []
        legend_elements = []
        for ms in df_ss['Microstate'].unique():
            df_ms = df_ss[df_ss['Microstate'] == ms]

            # Loop over age groups.
            x_coords = []
            for i_gr, gr in enumerate(age_groups):
                df_gr = df_ms[df_ms['AgeGroup'] == gr].copy()

                assert len(df_gr) == 1

                if metric == 'Coverage':
                    # To percentage.
                    df_gr[['Lower Bound', 'Upper Bound', 'Mean']] *= 100

                # Y-coords of patch.
                yc_i = [df_gr['Lower Bound'].values[0], df_gr['Upper Bound'].values[0]]

                # Draw patch.
                color = f'C{i_gr}'
                rect_i = Rectangle((xc, yc_i[0]), dx*0.85, yc_i[1]-yc_i[0],
                                   color=color, alpha=1)
                ax.add_patch(rect_i)
                legend_elements.append(Line2D([0], [0], color=color,
                                              lw=4, label=gr))

                # Draw mean.
                ax.plot((xc + xc+dx*0.85)/2 + np.array([-dx*0.85/2, dx*0.85/2]), [df_gr['Mean'].values]*2, color='k')
                x_coords.extend([xc, xc+1])

                # Update x-coord.
                xc += dx

            # Update X-coord (creates gap between MS).
            xc += 2.5*dx
            xticks.append(np.mean(x_coords))
            xlabels.append('M{}'.format('ABCDEFGH'.index(ms) + 1))

        # Set xticks and labels.
        plt.sca(ax)
        ax.set_title('{} ({})'.format(metric, ss))
        ax.set_xlabel('Microstate')
        plt.xticks(ticks=xticks, labels=xlabels)

        if metric == 'Duration':
            ax.set_ylabel('Duration (s)')
        elif metric == 'Occurrence':
            ax.set_ylabel('Occurrence (Hz)')
        elif metric == 'Coverage':
            ax.set_ylabel('Coverage (%)')

axes[0, -1].legend(handles=legend_elements[:len(age_groups)],
                   loc='center left', bbox_to_anchor=(0.8, 1))
for ax in axes[:-1, :].reshape(-1):
    ax.set_xlabel('')

if save_fig:
    fp_out = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures\fig_emm.eps'
    check_directory_exists(filepath=fp_out)
    plt.savefig(fp_out, format='eps')
    plt.savefig(fp_out.replace('.eps', '.tiff'), dpi=300)

if save_fig:
    save_fig_as(figname='fig_emm', directory=fig_dir, formats=('pdf', 'png', 'tiff'), info=__file__)
