"""
This script plots the features as function of PMA group and sleep stage.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr

from nnsa.utils import subplot_rows_columns
from nnsa.utils.dataframes import AnalyzerDf
from nnsa.utils.plotting import scale_figsize, save_fig_as

plt.close('all')
try:
    plt.style.use('ms_paper')
except OSError:
    pass

import matplotlib.font_manager as font_manager
font_dir = [r'C:\Users\thermans\.matplotlib\fonts']
for font in font_manager.findSystemFonts(font_dir):
    font_manager.fontManager.addfont(font)

#%% Settings.
# Filepath (.csv) to features (from collect_mean_features.py).
features_dir = r'D:\data\processed\microstates\features'
feature_filepath = os.path.join(features_dir, 'channel_average', 'features.csv')

# Rename columns.
new_names = {
    'GEV_4': 'GEV (%)',
    'Hurst_4': 'Hurst exponent (-)',
    'mean_duration': 'Mean duration (s)',
    'mean_occurrence': 'Mean occurrence (Hz)',
    'sleep_stage': 'Sleep stage',
    'PMA_group': 'PMA group',

}

# Features to boxplot.
features_box = [
    'Mean duration (s)', 'Mean occurrence (Hz)', 'Hurst exponent (-)',
    'P_rel_delta_low', 'P_rel_delta', 'P_rel_theta',
    'P_rel_alpha', 'P_rel_beta', 'SEF_90']

# Features to corrplot.
features_corr = [
    'Mean duration (s)', 'Mean occurrence (Hz)', 'Hurst exponent (-)',
    'P_rel_delta_low', 'P_rel_delta', 'P_rel_theta',
    'P_rel_alpha', 'P_rel_beta', 'SEF_90']

# Features to regplot.
features_reg = ['Mean duration (s)', 'Mean occurrence (Hz)', 'Hurst exponent (-)']

# For saving.
fig_width = 12
fig_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures'
save_fig = True

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Load.
df = pd.read_csv(feature_filepath, index_col=0).rename(columns=new_names)

#%% Plot group-level results.
if features_box:
    nrows, ncols = subplot_rows_columns(len(features_box))
    fig, axes_ = plt.subplots(nrows, ncols, tight_layout=True, sharex='all', squeeze=False)
    axes = np.reshape([axes_], -1)

    for ax, col in zip(axes, features_box):
        sns.boxplot(x='Sleep stage', y=col, data=df, hue='PMA group', ax=ax,
                    order=['QS', 'NQS'],
                    hue_order=['<=31', '32-33', '34-36', '>=37'], showfliers=False,
                )
        ax.set_xlabel('')
        ax.get_legend().remove()
        ax.set_title(col)
        if ax == axes_[0, -1]:
            ax.legend(bbox_to_anchor=(1, 1), loc='upper left', title='PMA group')

#%% Plot GEV.
fig, ax = plt.subplots(1, 1, tight_layout=True,
                       figsize=scale_figsize((5, 4), width=fig_width*0.8, unit='cm'),
                       )

sns.boxplot(x='Sleep stage', y='GEV (%)', data=df, hue='PMA group', ax=ax,
            order=['QS', 'NQS'],
            hue_order=['<=31', '32-33', '34-36', '>=37'], showfliers=False)
ax.set_xlabel('')
ax.get_legend().remove()
ax.legend(bbox_to_anchor=(1, 1), loc='upper left', title='PMA group')

if save_fig:
    save_fig_as(figname='GEV', directory=fig_dir, info=__file__)

#%% Plot differences between sleep stages.
nrows, ncols = subplot_rows_columns(len(features_box))
fig, axes_ = plt.subplots(nrows, ncols, tight_layout=True, sharex='all', squeeze=False,
                          figsize=scale_figsize((9.5, 6.5), width=16, unit='cm'),
                          )
axes = np.reshape([axes_], -1)

for ax, col in zip(axes, features_box):
    sns.boxplot(hue='Sleep stage', y=col, data=df, x='PMA group', ax=ax,
                order=['<=31', '32-33', '34-36', '>=37'],
                hue_order=['QS', 'NQS'], showfliers=False,
            )
    ax.get_legend().remove()
    ax.set_title(col)
    if ax == axes_[0, -1]:
        ax.legend(bbox_to_anchor=(1, 1), loc='upper left', title='Sleep state')
    if ax not in axes_[-1, :]:
        ax.set_xlabel('')

if save_fig:
    save_fig_as(figname='box_sleep_stages', directory=fig_dir, info=__file__)

#%% Differences between sleep stages per age.
df_qs = df[df['Sleep stage'] == 'QS']
df_nqs = df[df['Sleep stage'] == 'NQS']
df_diff = df_qs.select_dtypes(include=np.number) - df_nqs.loc[df_qs.index, :].select_dtypes(include=np.number)
df_diff['PMA group'] = df_qs.loc[df_qs.index, 'PMA group']
df_diff['PMA'] = df_qs.loc[df_qs.index, 'PMA']

features_diff = features_box[:3]
nrows, ncols = subplot_rows_columns(len(features_diff))
fig, axes_ = plt.subplots(nrows, ncols, tight_layout=True, sharex='all', squeeze=False,
                          figsize=scale_figsize((9.5, 4.5), width=14, unit='cm'),
                          )
axes = np.reshape([axes_], -1)
for ax, col in zip(axes, features_diff):
    sns.boxplot(y=col, data=df_diff, x='PMA group', ax=ax,
                order=['<=31', '32-33', '34-36', '>=37'], showfliers=False,
            )
    ax.set_title(col)
    if ax not in axes_[-1, :]:
        ax.set_xlabel('')

if save_fig:
    save_fig_as(figname='box_diff_sleep_stages', directory=fig_dir, info=__file__)

#%% Regression differences QS and NQS.
features_diff = features_box[:3]
nrows, ncols = subplot_rows_columns(len(features_diff))
fig, axes_ = plt.subplots(nrows, ncols, tight_layout=True, sharex='all', squeeze=False,
                          figsize=scale_figsize((9.5, 4.5), width=16, unit='cm'),
                          )
axes = np.reshape([axes_], -1)
for ax, col in zip(axes, features_diff):
    sns.regplot(y=col, data=df_diff, x='PMA', ax=ax,
            )
    ax.set_title(col)
    if ax not in axes_[-1, :]:
        ax.set_xlabel('')

    # Compute pearson correlation.
    r, p = pearsonr(df_diff['PMA'], df_diff[col])

    # ax.set_title('{}\nr={:.2f}, p={:.2g}'.format(sleep_stage, r, p))
    ax.set_title('{} (QS-NQS)\nr={:.2f}, p={:.2g}'.format(col.split('(')[0].strip(), r, p))

erer
#%% Regression features.
fig, axes = plt.subplots(len(features_reg), 2, tight_layout=True,
                         figsize=scale_figsize(np.array((5, 5.5 * len(features_reg) / 3 + 1)), width=fig_width, unit='cm'),
                         sharex='all', sharey='row', squeeze=False)
for i_y, y in enumerate(features_reg):
    for i_ss, sleep_stage in enumerate(['QS', 'NQS']):
        ax = axes[i_y, i_ss]
        df_ss = df[df['Sleep stage'] == sleep_stage]
        sns.regplot(x='PMA', y=y, data=df_ss, ax=ax)
        ax.set_xlabel('PMA (weeks)')
        if ax in axes[:, 0]:
            # ax.set_ylabel('Hurst exponent (-)')
            pass
        else:
            ax.set_ylabel('')
        if ax not in axes[-1, :]:
            ax.set_xlabel('')

        # Compute pearson correlation.
        r, p = pearsonr(df_ss['PMA'], df_ss[y])

        # ax.set_title('{}\nr={:.2f}, p={:.2g}'.format(sleep_stage, r, p))
        ax.set_title('{} ({})\nr={:.2f}, p={:.2g}'.format(y.split('(')[0].strip(), sleep_stage, r, p))

if save_fig:
    save_fig_as(figname='pma_regression', directory=fig_dir, info=__file__)

#%% Analyzer.
analyzer_qs = AnalyzerDf(df=df[df['Sleep stage'] == 'QS'], corr_method='pearson')
analyzer_nqs = AnalyzerDf(df=df[df['Sleep stage'] == 'NQS'], corr_method='pearson')
fig, axes = plt.subplots(2, 1, tight_layout=True, sharex='all',
                         figsize=scale_figsize((9.1, 5.6), width=fig_width, unit='cm'),
                         )
ax = axes[0]
analyzer_qs.corrbarplot(x='PMA', y_columns=features_corr, ax=ax)
ax.set_title('QS')
ax = axes[1]
analyzer_nqs.corrbarplot(x='PMA', y_columns=features_corr, ax=ax)
ax.set_title('NQS')

if save_fig:
    save_fig_as(figname='corrbarplot', directory=fig_dir, info=__file__)


for ax in axes:
    ax.grid()

size_scale = 350
fig, axes = plt.subplots(2, 1, tight_layout=True, sharex='all',
                         figsize=scale_figsize((6, 9), width=fig_width, unit='cm'),
                         )
ax = axes[0]
analyzer_qs.corrplot(
    x_columns=features_corr,
    y_columns=features_corr, add_annotations=2, ax=ax, size_scale=size_scale)
ax.set_title('QS')
ax = axes[1]
analyzer_nqs.corrplot(
    x_columns=features_corr,
    y_columns=features_corr, add_annotations=2, ax=ax, size_scale=size_scale)
# ax.set_yticklabels([])
ax.set_title('NQS')

if save_fig:
    save_fig_as(figname='corrmatplot', directory=fig_dir, info=__file__)
