"""
This script plots EMM for the QS metrics.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

#%% Settings.
# Directory containing the Excel files with the estimated marginal means (as computed by SPSS).
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'

# Sleep stage.
ss = 'QS'

#%% Process.
age_groups = ['<=31', '32-33', '34-36', '>=37']

# Init fig.
fig, axes = plt.subplots(1, 3, tight_layout=True, sharex='all', figsize=2.3*np.array((4.46, 1.3)))
axes = np.reshape([axes], -1)

# Loop over metrics.
for i_ax, (metric, ax) in enumerate(zip(['Duration', 'Occurrence', 'Coverage'], axes)):
    # Load results.
    fp = os.path.join(output_dir, f'emm_{metric.lower()}.xlsx')
    df = pd.read_excel(fp).fillna(method='ffill')

    # Select data.
    df_ss = df[df['SleepStage'] == ss]

    # Initiate x-coord.
    xc = 0

    # Loop over microstates.
    xticks = []
    xlabels = []
    legend_elements = []
    for ms in df_ss['Microstate'].unique():
        df_ms = df_ss[df_ss['Microstate'] == ms]

        # Loop over age groups.
        x_coords = []
        for i_gr, gr in enumerate(age_groups):
            df_gr = df_ms[df_ms['AgeGroup'] == gr]

            if metric == 'Coverage':
                # To percentage.
                df_gr[['Lower Bound', 'Upper Bound', 'Mean']] *= 100

            # Y-coords of patch.
            yc_i = [df_gr['Lower Bound'].values, df_gr['Upper Bound'].values]

            # Draw patch.
            color = f'C{i_gr}'
            rect_i = Rectangle((xc, yc_i[0]), 1, yc_i[1]-yc_i[0],
                               color=color, alpha=1)
            ax.add_patch(rect_i)
            legend_elements.append(Line2D([0], [0], color=color,
                                          lw=4, label=gr))

            # Draw mean.
            ax.plot([xc, xc+1], [df_gr['Mean'].values]*2, color='k')
            x_coords.extend([xc, xc+1])

            # Update x-coord.
            xc += 1

        # Update X-coord (creates gap between MS).
        xc += 2
        xticks.append(np.mean(x_coords))
        xlabels.append(ms)

    # Set xticks and labels.
    plt.sca(ax)
    ax.set_title(metric)
    ax.set_xlabel('Microstate')
    plt.xticks(ticks=xticks, labels=xlabels)

    if metric == 'Duration':
        ax.set_ylabel('Duration (s)')
    elif metric == 'Occurrence':
        ax.set_ylabel('Occurrence (Hz)')
    elif metric == 'Coverage':
        ax.set_ylabel('Coverage (%)')

axes[-1].legend(handles=legend_elements[:len(age_groups)],
                loc='center left', bbox_to_anchor=(0.8, 1))