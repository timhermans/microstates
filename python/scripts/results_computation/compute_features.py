"""
This script computes (spectral) EEG features for all recordings.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import pyprind
from scipy import signal
import seaborn as sns

from nnsa import get_filepaths, EegDataset
from nnsa.utils import check_directory_exists
from nnsa.utils.mathematics import nextpow2

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

#%% Settings.
# Directory with preprocessed .mat files.
preprocessed_dir = r'D:\data\processed\microstates\preprocessed'
epochs_dir = os.path.join(preprocessed_dir, 'epochs')

# Output directory.
output_dir = r'D:\data\processed\microstates\features'

# Find all .mat files.
all_filepaths = get_filepaths(directory=epochs_dir, pattern='*.mat', subdirectories=False)

# Freq bands for absolute and relative power features.
freq_bands = {
    'delta_low': (0, 0.5),
    'delta': (0.5, 4),
    'theta': (4, 8),
    'alpha': (8, 13),
    'beta': (13, 25),
}

#%% Loop over recordings.
print(f'Found {len(all_filepaths)} recordings.')
check_directory_exists(directory=output_dir)

bar = pyprind.ProgBar(len(all_filepaths))
for ii, fp in enumerate(all_filepaths):
    filename = os.path.splitext(os.path.basename(fp))[0]
    rec_name, sleep_stage = filename.split('_preprocessed_')

    # Output filepath.
    fp_out = os.path.join(output_dir, f'{rec_name}_features_{sleep_stage}.csv')

    if os.path.exists(fp_out):
        msg = f'File {fp_out} already exists. Skipping...'
        print(msg)
        continue

    # Load EEG.
    eeg_ds = EegDataset.read_mat(fp)

    # To array.
    eeg, channel_labels = eeg_ds.asarray(channels_last=False, return_channel_labels=True)
    fs = eeg_ds.fs

    # Compute features (per channel).
    feature_data = dict()

    # PSD.
    freqs, Pxx = signal.welch(
        eeg, fs=fs, window='hamming', nperseg=fs*4, noverlap=None,
        nfft=int(2 ** nextpow2(max(fs * 8, eeg.shape[-1]))), detrend='constant', axis=-1)
    P_rel = Pxx / np.sum(Pxx, axis=-1, keepdims=True)
    P_cum = np.cumsum(P_rel, axis=-1) * 100  # In percentage

    # Spectral edge frequency.
    for edge in [90, 95]:
        idx_edge = np.argmin(np.abs(P_cum - edge), axis=-1)
        sef = freqs[idx_edge]
        feature_data[f'SEF_{edge}'] = sef

    # Spectral powers in bands.
    P_tot = np.trapz(Pxx, freqs, axis=-1)
    for band, (f_low, f_high) in freq_bands.items():
        idx_band = np.logical_and(freqs >= f_low, freqs <= f_high)
        abs_pow = np.trapz(Pxx[:, idx_band], freqs[idx_band], axis=-1)
        rel_pow = abs_pow / P_tot

        feature_data[f'P_abs_{band}'] = abs_pow
        feature_data[f'P_rel_{band}'] = rel_pow

    # To DataFrame.
    channel_labels = [c.replace('EEG', '').strip() for c in channel_labels]
    df = pd.DataFrame(feature_data, index=pd.Index(channel_labels, name='Channel'))

    df.to_csv(fp_out, index=True)
    bar.update()
