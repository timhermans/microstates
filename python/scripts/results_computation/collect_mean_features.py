"""
This script loads the features of each recording, averages the channels and puts
the averages per recording in a new dataframe.
If available, also loads saved GEV values and Hurst exponents (as created by Matlab script).

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import pyprind
import seaborn as sns

from microstates.data_paths import get_recording_data
from nnsa import get_filepaths
from nnsa.utils import check_directory_exists

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

#%% Settings.
# Directory with feature files (.csv) per recording.
features_dir = r'D:\data\processed\microstates\features'

# Filepath to csv with GEV values etc. to append.
filepath_gev = r'D:\data\processed\microstates\features\channel_average\GEV_hurst.csv'
filepath_gev_group = r'D:\data\processed\microstates\features\channel_average\GEV_hurst_group_level.csv'

# Filepath to mean duration and occurrence.
filepath_mean_dur = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results\individual_results_mean.csv'

# Output filepath (.csv).
filepath_out = os.path.join(features_dir, 'channel_average', 'features.csv')

# Find all .csv files.
all_filepaths = get_filepaths(directory=features_dir, pattern='*.csv', subdirectories=False)

#%% Loop over recordings.
print(f'Found {len(all_filepaths)} feature files.')
check_directory_exists(filepath=filepath_out)

# Load recording data.
df_rd = get_recording_data(add_filepaths=False)
df_rd.index = pd.Index(df_rd.filename.apply(lambda x: os.path.splitext(x)[0]), name='rec_name')

# Load GEV.
other_features = pd.concat([
    pd.read_csv(filepath_gev_group, index_col=0)[['sleep_stage', 'GEV_4']],
    pd.read_csv(filepath_gev, index_col=0)[['Hurst_4']],
], ignore_index=False, axis=1)

qs_features = other_features[other_features.sleep_stage == 'QS'].drop(columns=['sleep_stage'])
nqs_features = other_features[other_features.sleep_stage == 'NQS'].drop(columns=['sleep_stage'])

# Load mean durations.
df_mean_dur = pd.read_csv(filepath_mean_dur)
df_mean_dur.index = df_mean_dur.DataSet.apply(lambda x: x.split('_preprocessed_')[0]).rename('rec_name')
df_mean_dur = df_mean_dur[['MeanDuration', 'MeanOccurrence', 'SleepStage']].rename(columns={
    'SleepStage': 'sleep_stage',
    'MeanDuration': 'mean_duration',
    'MeanOccurrence': 'mean_occurrence',
})

qs_features = pd.concat([qs_features, df_mean_dur[df_mean_dur.sleep_stage == 'QS']], ignore_index=False, axis=1).drop(columns=['sleep_stage'])
nqs_features = pd.concat([nqs_features, df_mean_dur[df_mean_dur.sleep_stage == 'NQS']], ignore_index=False, axis=1).drop(columns=['sleep_stage'])

bar = pyprind.ProgBar(len(all_filepaths))
all_series = []
for ii, fp in enumerate(all_filepaths):
    filename = os.path.splitext(os.path.basename(fp))[0]
    rec_name, sleep_stage = filename.split('_features_')

    df_i = pd.read_csv(fp, index_col=0)

    features_i = pd.Series(
        {'sleep_stage': sleep_stage}, name=rec_name)
    features_i = pd.concat([features_i, df_rd.loc[rec_name, ['PMA_group', 'PMA']]])
    features_i = pd.concat([features_i, df_i.mean()])

    extra_features = None
    if filepath_gev is not None:
        if sleep_stage == 'QS':
            if rec_name in qs_features.index:
                extra_features = qs_features.loc[rec_name, :]
        elif sleep_stage == 'NQS':
            if rec_name in nqs_features.index:
                extra_features = nqs_features.loc[rec_name, :]
        else:
            raise NotImplementedError(sleep_stage)

    if extra_features is not None:
        features_i = pd.concat([features_i,extra_features])

    all_series.append(features_i.rename(rec_name))

# Concatenate all series into a dataframe.
df = pd.concat(all_series, axis=1).T.dropna()
df.index.name = 'rec_name'

# Save.
df.to_csv(filepath_out)

