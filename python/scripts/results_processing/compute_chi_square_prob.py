"""
This script implements the randomnization test on the chi-square metric for MS syntax.
Prints p-values that test the hypothesis that syntax is random.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

plt.close('all')
plt.style.use('pres')

#%% Settings.
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'
n_maps = 4
n_randomnizations = 5000

#%% Load.
df = pd.read_csv(os.path.join(output_dir, 'results_mean.csv'))


#%% Functions.
def compute_chi_square(df, n_randomnizations=5000, seed=43):
    # Compute chi-square.
    chi_square = 0
    chi_square_random = np.zeros(n_randomnizations)
    for i in range(1, n_maps + 1):
        for j in range(1, n_maps + 1):
            if i != j:
                p_org = df[f'OrgTM_{i}->{j}'].values
                p_exp = df[f'ExpTM_{i}->{j}'].values

                # Compute chi-square distance.
                p_org_mean = np.nanmean(p_org)
                p_exp_mean = np.nanmean(p_exp)
                chi_square += ((p_org_mean - p_exp_mean) ** 2) / p_exp_mean

                if n_randomnizations:
                    # Init.
                    np.random.seed(seed)
                    n = len(p_org)
                    p_mat = np.vstack([p_org, p_exp])

                    # Shuffle org and exp labels.
                    p_org_all = []
                    p_exp_all = []
                    for _ in range(n_randomnizations):
                        idx_org_new = np.random.randint(0, high=2, size=n)
                        idx_exp_new = np.ones_like(idx_org_new) - idx_org_new
                        p_org = p_mat[idx_org_new, np.arange(n)]
                        p_exp = p_mat[idx_exp_new, np.arange(n)]
                        assert (np.all((p_mat.sum(0)) == (p_org + p_exp)))
                        p_org_all.append(p_org)
                        p_exp_all.append(p_exp)

                    # Take mean and compte chi-square
                    p_org_all = np.vstack(p_org_all)
                    p_exp_all = np.vstack(p_exp_all)
                    p_org_mean = np.nanmean(p_org_all, axis=-1)
                    p_exp_mean = np.nanmean(p_exp_all, axis=-1)
                    chi_square_random += ((p_org_mean - p_exp_mean) ** 2) / p_exp_mean

    # Compute p-value.
    pval = np.mean(chi_square_random >= chi_square)

    return chi_square, pval


#%% Process.
# Loop over sleep stage and age group.
for ss in ['QS', 'NQS']:
    print(f'Processing sleep stage {ss}...')
    df_ss = df[df['SleepStage'] == ss]
    for gr in ['<=31', '32-33', '34-36', '>=37']:
        print(f'Processing age group {gr}...')
        df_gr = df_ss[df_ss['AgeGroup'] == gr]

        # Compute true chi-square.
        chi_square, pval = compute_chi_square(df_gr, n_randomnizations=n_randomnizations)

        if pval != 0:
            print(f'p-value = {pval:.5f}')
        else:
            print(f'p-value <{1/n_randomnizations}')

