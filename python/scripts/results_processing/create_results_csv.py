"""
This script loads all results files and creates csv files with results
in long format.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from microstates.data_paths import get_recording_data
from microstates.path_config import OUTPUT_DIR

plt.close('all')
plt.style.use('pres')


#%% Settings.
# Whether to create the files for metrics found with individual (False) or group level (True) MS maps
# (the name of the output file will be different depending on this, so no overwriting will be done).
use_group_results = False

# Whether to use the metrics computed using GFP (True) or continuous (False) backfit.
gfp_backfit = False

# Data selection.
outcome = 'normal'
sleep_stages_all = ['QS', 'NQS']
pma_groups_all = ['<=31', '32-33', '34-36', '>=37']

# Names for the maps.
map_names = {
    1: 'A',
    2: 'B',
    3: 'C',
    4: 'D',
}

# Whether to save the files automatically when running this script.
save = True

#%% Set paths.
# Path to directory with the metrics (csv) files.
if use_group_results:
    metrics_dir = os.path.join(OUTPUT_DIR, 'epochs', 'microstates', 'group_level', 'sorted')
else:
    metrics_dir = os.path.join(OUTPUT_DIR, 'epochs', 'microstates')
output_dir = os.path.join(metrics_dir)

#%% Load metrics.
df = pd.DataFrame()
for ss in sleep_stages_all:
    for i_pma, pma in enumerate(pma_groups_all):
        # Determine filename for the current PMA-sleep group.
        if use_group_results:
            filename = f'GroupMean_Metrics_GFP_{outcome}_{i_pma+1}_{ss}.csv'
        else:
            filename = f'Metrics_GFP_{outcome}_{i_pma+1}_{ss}.csv'
        if not gfp_backfit:
            # Remove GFP from filename.
            filename = filename.replace('GFP_', '')

        # Read and collect results.
        df_i = pd.read_csv(os.path.join(metrics_dir, filename))
        df_i = df_i.rename(columns={'Subject': 'Recording name'})
        df_i['ID'] = df_i.apply(lambda row: int(row['Recording name'].split('_')[0]), axis=1)
        df_i = df_i[['ID']+df_i.columns.tolist()[:-1]]
        df = pd.concat([df, df_i], ignore_index=True)

new_columns = [col.replace('Contribution', 'Coverage') for col in df.columns]
df.columns = new_columns

# Add PMA.
df_rd = get_recording_data()
indices = [q.lower().replace('pt', '').replace('.edf', '').strip() for q in df_rd['filename']]
df_rd.index = indices
pmas = df_rd['PMA']
df['PMA'] = df.apply(lambda row: pmas[row['Recording name']], axis=1)

# Rename some columns.
df = df.rename(columns={'Condition': 'SleepStage',
                        'Group': 'AgeGroup',
                        'Recording name': 'RecordingName'})

if save:
    # Save.
    if use_group_results:
        filepath_out = os.path.join(output_dir, 'results_mean.csv')
    else:
        filepath_out = os.path.join(output_dir, 'individual_results_mean.csv')
    df.to_csv(filepath_out, index=False)

#%% Loop over MS and create long format for duration, occurrence and coverage (#recordings x #microstates).
df_m = pd.DataFrame()
for i_map in range(1, 5):
    df_i = df[['ID',
               'DataSet',
               'RecordingName',
               'PMA',
               'AgeGroup',
               'SleepStage',
               'Template',
               'ExpVar',
               'SortInfo',
               'TotalTime']]
    df_i['Microstate'] = map_names[i_map]
    for metric in ['Duration', 'Occurrence', 'Coverage']:
        col = f'{metric}_{i_map}'
        if col in df:
            df_i[metric] = df[col]
        else:
            print(f'Warning: column {col} not in df.')
    df_m = pd.concat([df_m, df_i], ignore_index=True)

if save:
    # Save.
    if use_group_results:
        filepath_out = os.path.join(output_dir, f'results_per_ms.csv')
    df_m.to_csv(filepath_out, index=False)


#%% Create long format recordings x microstate pair for directional predominance
# (# recordings x (#microstates x (#microsates -1))/2).
def get_predominance(df, a, b, map_names):
    col_a_b = 'OrgTM_{}->{}'.format(a, b)
    col_b_a = 'OrgTM_{}->{}'.format(b, a)
    predom = df[col_a_b] - df[col_b_a]
    df_i = df[['ID',
               'DataSet',
               'RecordingName',
               'PMA',
               'AgeGroup',
               'SleepStage',
               'Template',
               'ExpVar',
               'SortInfo',
               'TotalTime']]
    df_i['Microstates'] = '{}<->{}'.format(map_names[a], map_names[b])
    df_i['DirectionalPredominance'] = predom
    return df_i


# Loop over all microstate pairs.
all_maps = list(map_names.keys())
df_dp = pd.DataFrame()
for i_a, map_a in enumerate(all_maps):
    for map_b in all_maps[i_a+1:]:
        df_i = get_predominance(df, map_a, map_b, map_names)
        df_dp = pd.concat([df_dp, df_i], ignore_index=True)

if save:
    # Save.
    if use_group_results:
        filepath_out = os.path.join(output_dir, f'results_dp.csv')
    df_dp.to_csv(filepath_out, index=False)

