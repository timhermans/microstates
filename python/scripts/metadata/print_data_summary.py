"""
This script prints a summary of the data (number of neonates, etc.).

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np

import pandas as pd

from microstates.data_paths import get_recording_data


def median_iqr(x):
    q25, q50, q75 = np.nanpercentile(x, [25, 50, 75])
    return f'{q50} ({q25} - {q75})'


# Load recording data, dropping rows that were not used.
df = get_recording_data().dropna(subset=['PMA_group'])

# Only get the normals.
df = df[df['outcome'] == 'normal']

# Only get the first entry of a patient.
df_pd = pd.DataFrame()
for pat_num in df['pat_num'].unique():
    row = df[df['pat_num'] == pat_num].iloc[0:1, :]
    df_pd = pd.concat([df_pd, row], ignore_index=True)

#%% Number of patients and recordings.
num_patients = len(df_pd)
num_recordings = len(df)
print(f'Number of neonates: {num_patients}')
print(f'Number of recordings: {num_recordings}')
print(f'Number of recordings per age group: {df.groupby("PMA_group").count()["rec_num"]}')
print(f'Number of recordings per neonate (median + IQR): {median_iqr(df.groupby("pat_num").count()["rec_num"])}')

#%% Ages.
pma = df['PMA']
print(f'Min-max PMA: {pma.min(), pma.max()}')
