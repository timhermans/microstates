"""
This script creates an Excel sheet containing patient numbers, PMAs, GAs, outcome, dataset and filenames.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
from nnsa import EdfReader

from microstates.path_config import D1_DATA_DIR, D2_DATA_DIR

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

# Excel file for D1.
fp_d1 = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\metadata\preterm\preterm_patient_data_kirubin.xlsx'

# Excel file for sleep labels of D1 (contains GA).
fp_d1_qs = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\metadata\preterm\QS_labels_preterm_dataset.xlsx'

# Excel file for D2.
fp_d2 = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\metadata\abnormal_preterm\labels.xlsx'

# Location of EDFs D1.
dir_d1 = D1_DATA_DIR

# Location of EDFs D2.
dir_d2 = D2_DATA_DIR

# Initialize dataframe that will contain all available recordings.
df = pd.DataFrame()

#%% Loop over D1, find EDFs, if found, add to dataframe.
df_d1 = pd.read_excel(fp_d1)
df_d1_qs = pd.read_excel(fp_d1_qs)
for index, row in df_d1.iterrows():
    # Filepath of EDF.
    filename = row['Filename']
    fp_i = os.path.join(dir_d1, filename)
    file_exists = os.path.exists(fp_i)
    df_d1.loc[index, 'EDF exists'] = file_exists
    if file_exists:
        # Get duration.
        with EdfReader(fp_i) as r:
            duration = r.additional_info['total_duration']

        pat_num = row['pat_num']
        row_qs = df_d1_qs[df_d1_qs['Patnum'] == pat_num].iloc[0, :]
        ga = row_qs['GA (weeks)'] + row_qs['GA (days)']/7

        # Collect info.
        info_i = {
            'pat_num': pat_num,
            'rec_num': row['rec_num'],
            'GA': ga,
            'PMA': row['PMA'],
            'duration': pd.Timedelta(seconds=duration),
            'outcome': 'normal' if row['Normal'] == 1 else np.nan,
            'dataset': 'D1',
            'filename': filename,
        }
        df_i = pd.DataFrame(info_i, index=[0])
        df = pd.concat([df, df_i], ignore_index=True)

#%% Loop over D2, find EDFs, if found, add to dataframe.
df_d2 = pd.read_excel(fp_d2).dropna(subset=['pat_num']).reset_index()

for index, row in df_d2.iterrows():
    # Filepath of EDF.
    pat_num = row['pat_num']
    rec_num = row['rec_num']
    filename = 'PT {}_{}.EDF'.format(int(pat_num), int(rec_num))
    fp_i = os.path.join(dir_d2, filename)
    file_exists = os.path.exists(fp_i)
    df_d2.loc[index, 'EDF exists'] = file_exists
    if file_exists:
        # Get duration.
        with EdfReader(fp_i) as r:
            duration = r.additional_info['total_duration']

        # Get outcome.
        if row['normal'] == 1:
            outcome_i = 'normal'
        elif row['severe'] == 1:
            outcome_i = 'severe'
        elif row['mental'] == 1 or row['motor'] == 1:
            outcome_i = 'moderate'
        else:
            outcome_i = np.nan
            raise AssertionError

        # Collect info.
        info_i = {
            'pat_num': pat_num,
            'rec_num': rec_num,
            'PMA': row['PMA'],
            'duration': pd.Timedelta(seconds=duration),
            'outcome': outcome_i,
            'dataset': 'D2',
            'filename': filename,
        }
        df_i = pd.DataFrame(info_i, index=[0])
        df = pd.concat([df, df_i], ignore_index=True)


df['normal'] = df['outcome'] == 'normal'
# df.to_excel('recording_data_v2.xlsx', index=False)
