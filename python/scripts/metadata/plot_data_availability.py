"""
This script plots data availability (as function of PMA).

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""

import numpy as np
import matplotlib.pyplot as plt

from microstates.data_paths import get_recording_data

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

# Window size of PMA (in weeks).
d_pma = 1/6
linewidth = 5

colors = {
    'normal': 'C2',
    'moderate': 'C1',
    'severe': 'C3',
}

df = get_recording_data(add_filepaths=False)

df['order'] = df.apply(lambda x: np.argmax(x['outcome'] == np.array(['normal', 'mild', 'severe'])), axis=1)
df = df.sort_values('order', kind='mergesort')

pma = df['PMA']
outcome = df['outcome']
pma_min = pma.min()
pma_max = pma.max()

_, idx_pat_nums = np.unique(df['pat_num'], return_index=True)
all_pat_nums = df['pat_num'].values[np.sort(idx_pat_nums)]
df_pat = df.iloc[idx_pat_nums, :]
df_pat['num_recordings'] = df_pat.apply(lambda x: (df['pat_num'] == x['pat_num']).sum(), axis=1)

pma_all = np.arange(pma_min, pma_max + d_pma/100, d_pma)

# Matrix with numbers.
fig, ax = plt.subplots(1, 1, figsize=np.array((8, 5))*1.5, squeeze=True, tight_layout=True)
for i, pat_num in enumerate(all_pat_nums):
    pmas_i = pma[df['pat_num'] == pat_num].values
    outcome_i = outcome[df['pat_num'] == pat_num].values[0]
    color = colors[outcome_i]
    height = -(i + int(df['dataset'][df['pat_num'] == pat_num].values[0] == 'D2') * 2)
    if height % 2 == 0:
        plt.axhline(height, color=np.ones(3) * 0.85, linewidth=linewidth)
    else:
        plt.axhline(height, color=np.ones(3) * 0.95, linewidth=linewidth)

    for pma_i in pmas_i:
        plt.plot([pma_i - d_pma, pma_i + d_pma], [height, height], color=color, linewidth=linewidth)

plt.axhline(-len(np.unique(df['pat_num'][df['dataset'] == 'D1'])) - 0.5, color=np.ones(3)*0.4, linewidth=linewidth/2, linestyle='-')

plt.xlim([pma_min-d_pma, pma_max+d_pma])
plt.ylim([height-1, 1])
plt.xlabel('PMA (weeks)')
ax.get_yaxis().set_visible(False)
ax.spines['left'].set_visible(False)
xticks = np.array([pma_min, 30, 35, 40, 45, pma_max])
plt.xticks(xticks, xticks)
plt.grid()
