"""
This script ...

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
from matplotlib.patches import Rectangle

from microstates.data_paths import get_recording_data
from nnsa.utils import check_directory_exists

plt.close('all')
try:
    plt.style.use('ms_paper')
except OSError:
    pass

import matplotlib.font_manager as font_manager
font_dir = [r'C:\Users\thermans\.matplotlib\fonts']
for font in font_manager.findSystemFonts(font_dir):
    font_manager.fontManager.addfont(font)

#%% Settings.
# Directory containing the Excel files with the estimated marginal means (as computed by SPSS).
output_dir = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Results'
save_fig = True

if not save_fig:
    plt.rcParams['figure.dpi'] = 140

#%% Load.
df = get_recording_data()

# Only select normal.
df = df[df.outcome == 'normal']

#%% Plot.
# Print and plot number of recordings per outcome and age group.
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=0.7*np.array([6.5, 5]))
hue_order = ['<=31', '32-33', '34-36', '>=37']
sns.histplot(x='PMA', data=df, hue='PMA_group', hue_order=hue_order,
             ax=ax, binwidth=1, legend=False,
             binrange=(np.floor(df.PMA.min()),
                       np.floor(df.PMA.max())))

ax.set_ylim([0, 25])

counts = df['PMA_group'].value_counts()
print(counts)

# Add annotation boxes.
center = 35.7
width = 2.7
spacing = 0.2
xtext_all = np.arange(4)*(width+spacing) + (center - spacing*1.5 - 2*width)
ytext = 22
fontsize = 9
height = 3
for ii, (group, xtext) in enumerate(zip(hue_order, xtext_all)):
    tex = f'{group} wks\nn={counts[group]}'
    ax.text(xtext, ytext, tex, va='center', ha='center')
    rect = Rectangle(xy=(xtext - width/2, ytext-height*0.45), width=width, height=height,
                     color=f'C{ii}', alpha=0.5)
    ax.add_patch(rect)

ax.set_xlabel('PMA (weeks)')
ax.set_ylabel('Number of recordings (-)')

if save_fig:
    fp_out = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\PhD\Microstates\Paper\Figures\fig_pma_hist.eps'
    check_directory_exists(filepath=fp_out)
    plt.savefig(fp_out, format='eps')
    plt.savefig(fp_out.replace('.eps', '.tiff'), dpi=300)
