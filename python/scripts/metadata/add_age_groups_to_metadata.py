"""
This script adds age groups to the metadata Excel file.
Assings at most 1 recording per patient to one group. Remaining recordings are set to NaN.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from microstates.data_paths import get_recording_data

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass


def assign_group(row):
    pma = np.floor(row['PMA'])

    if row['usability'] == 0:
        group = np.nan
    elif pma <= 31:
        group = '<=31'
    elif pma <= 33:
        group = '32-33'
    elif pma <= 36:
        group = '34-36'
    else:  # >= 37
        group = '>=37'
    return group


# Specify target PMA for each group (used to select best recording if multiple are available per patient).
# If set as None, the median group value is taken.
median_pmas = {
    '<=31': 30,
    '32-33': 33,
    '34-36': 35.5,
    '>=37': 39
}

# Read excel.
df = get_recording_data(add_filepaths=False)

# Set filename as index.
df = df.set_index('filename')

# Assign PMA groups.
df['PMA_group'] = df.apply(assign_group, axis=1)

# Plot distribution.
plt.figure(tight_layout=True)
ax0 = plt.subplot(111)
hue_order = ['<=31', '32-33', '34-36', '>=37']
sns.histplot(x='PMA', data=df, hue='PMA_group', hue_order=hue_order,
             ax=ax0, binwidth=1, binrange=(np.floor(df.PMA.min()),
                                           np.floor(df.PMA.max())))
plt.title('Initial grouping')

# Loop over age groups and remove multiple recordings of the same patient.
for group in df['PMA_group'].dropna().unique():
    df_group = df[df['PMA_group'] == group]

    # Target PMA.
    if median_pmas is not None and median_pmas[group] is not None:
        median_pma = median_pmas[group]
    else:
        median_pma = df_group['PMA'].median()

    for pat_num in df_group['pat_num'].unique():
        df_pat = df_group[df_group['pat_num'] == pat_num]

        if len(df_pat) > 1:
            # Select the one with highest usability and closest to median PMA.
            df_highest_usability = df_pat[df_pat['usability'] == df_pat['usability'].max()]
            df_best = df_highest_usability[(df_highest_usability['PMA'] - median_pma).abs() == (
                    df_highest_usability['PMA'] - median_pma).abs().min()]

            # Remove others from PMA group.
            index_remove = set(df_pat.index) - set(df_best.index[0:1])  # Take the first one.
            for idx in index_remove:
                df.at[idx, 'PMA_group'] = np.nan

df = df.reset_index()
# df.to_excel('recording_data.xlsx', index=False)

